<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>制定任务</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<script type="text/javascript"> 
	$(document).ready(function(e) {
		$(".select1").uedSelect({
			width : 118			  
		});
	});
	</script>

</head>

<body>
<form action="TaskServlet?method=saveTask" method="post">
<div class="MainContent">

	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">新任务</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>
	
	<div class="mainindex">
		<div class="ItemBlock">
			<table border="0" cellspacing="1" cellpadding="1" class="dateTable">
				<tr>
					<th width="12%">任务名称</th>
					<td colspan="3"><input type="text" name="task_name" class="txtInput"/> </td>
				</tr>
				<tr>
					<th>任务描述</th>
					<td colspan="3"> <textarea name="task_describe" class="TxtArea" ></textarea></td>
				</tr>
				<tr>
					<th>开始时间</th>
				  <td width="33%"><input type="date" name="task_begin_time1" class="txtInput2"></td>
					<th width="12%">结束时间</th>
				  <td width="43%"><input type="date" name="task_end_time1" class="txtInput2" /></td>
				</tr>
				<tr>
					<th >实施人</th>
					<td>
						<select name="user_id" class="select1">
						<c:forEach items="${list }" var="user">
							<option value="${user.id }">${user.username }</option>
						</c:forEach>
						</select>
					</td>
					<th >任务状态</th>
					<td>
					<input type="hidden" name="task_state" value="0"/>
					<input name="" type="text" class="txtInput2" value="未实施" disabled="disabled"/>
					</td>
				</tr>
			</table>
		</div>
		<div class="buttonArea">	   
	   <input name="" type="button" value="重置" class="InfoButton2" onclick="this.form.reset();return false;"/>
	   <input name="" type="submit" value="提交" class="InfoButton2" />
	   </div>	
	</div> 
  

</div>

</form>
</body>
</html>
