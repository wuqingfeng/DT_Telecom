﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>任务详情</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>

<body>

<div class="MainContent">

	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">任务详细信息</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>

  <div class="mainindex">
		
		<%@ include file="/WEB-INF/include/detailedtask.jsp"%>
		
		<%-- <div class="ItemBlock">
		 <table border="0"cellspacing="1" cellpadding="1" class="dateTable" >
			<tr>
			  <th width="13%" align=center id="TableTitle1" scope="row">任务名称</th>
			  <td colspan="3"><input name="" type="text" class="txtInput" value="${task.task_name }"  disabled="disabled"/></td>
		   </tr>
		   <tr>
			  <th >任务描述</th>
			  <td colspan="3"><textarea name="" rows="2" class="TxtArea" >${task.task_describe }</textarea></td>
		   </tr>
			<tr>
			  <th >开始时间</th>
			  <td width="32%"><input name="" type="text" class="txtInput2" value="${task.task_begin_time }"  disabled="disabled"/></td>
			  <th width="13%">结束时间</th>
			  <td width="42%"><input name="" type="text" class="txtInput2" value="${task.task_end_time }"  disabled="disabled"/></td>
			</tr>
			<tr>
			  <th >实际开始时间</th>
			  <td><input name="" type="text" class="txtInput2" value=""  disabled="disabled"/></td>
			  <th  >实际结束时间</th>
			  <td><input name="" type="text" class="txtInput2" value=""  disabled="disabled"/></td>
			</tr>
			<tr>
			  <th>实施人</th>
			 <td><input name="" type="text" class="txtInput2" value="${task.user_id }"  disabled="disabled"/></td>
			 <th>任务状态</th>
			 <td><input name="" type="text" class="txtInput2" value="${task.task_state }"  disabled="disabled"/></td>
		   </tr>
		</table>
		</div> --%>
	
		<div class="MainNullArea"> </div> 
		  
		<div class="MainTitle2">实施计划</div>
	
        <%@ include file="/WEB-INF/include/feedbacklist.jsp"%>
        
        
		<div class="buttonArea">
		   <input id="planbutton" name="" type="button" value="计划详情" class="InfoButton"/>
		   <input name="" type="button" value="返回" class="InfoButton2" onclick="javascript:history.go(-1);"/>
		</div>	
		<script type="text/javascript">
			$("#planbutton").click(function(){
				
				var plan=$(":input:checked");
				var planId=plan.attr("name"); 
				if(plan.size() == 1){
					if(planId != null && planId != "" ){
						window.location="TaskServlet?method=detailedplaninformation&planId="+planId;
					}else{
						alert("请选择计划 ！");
					} 
				}else{
					alert("请选择一项内容");
				}
				
				
				
			});
		
		
		</script>	
  </div>


</div>
 

</body>
</html>
