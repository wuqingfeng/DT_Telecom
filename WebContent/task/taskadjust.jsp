<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>调整任务</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   
</head>
<body>

<div class="MainContent">

	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">任务调整</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>
	
	<div class="mainindex"> 
	
		<%@ include file="/WEB-INF/include/tasklist.jsp"%>
	
		<div class="buttonArea">
		
		<input id="deltask"  type="button" value="删除" class="InfoButton2" />
		<input id="adjusttask"  type="button" value="任务调整" class="InfoButton" />
		</div>
		
		 <script type="text/javascript">
	   		$("#deltask").click(function(){
	   			var taskId=$(":input:radio:checked").val();
	   			if(taskId !=null && taskId!= ""){
	   				window.location="TaskServlet?method=deleteTask&taskId="
	   					+taskId;
	   			}else{
	   				alert("请选择任务");
	   			}
	   			
	   		});
	   		
	   		$("#adjusttask").click(function(){
	   			var taskId=$(":input:radio:checked").val();
	   			if(taskId !=null && taskId!= ""){
	   				window.location="TaskServlet?method=detailedtaskinformation&taskId="
	   					+taskId+"&target=taskadjustinformation";
	   			}else{
	   				alert("请选择任务");
	   			}
	   			
	   		});
	   </script>
			
	</div>
	
	
	  
	   
	
 
</div>


</body>
</html>
