<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>我的任务</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>

<body>

<div class="MainContent">

	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">我的任务</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>
	
	<div class="mainindex">
	
	   <%@ include file="/WEB-INF/include/tasklist.jsp"%>
	   
	   <div class="buttonArea">
		<input id="taskbtn" type="button"name="" value="制定计划" class="InfoButton" />
		<!--  onclick="parent.rightFrame.location.href='../plan/myplan.html'"   -->
		</div>
		
		<script type="text/javascript">
			$("#taskbtn").click(function(){
				var taskId=$(":input:radio:checked").val();
			//	alert(taskId);
				if(taskId !=null && taskId != "" ){
					window.location="PlanServlet?method=detailedTaskAndPlan&taskId="+taskId;
				}
			});
		
		
		
		</script>
		
	</div>


</div>


</body>
</html>
