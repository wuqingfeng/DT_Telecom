<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>调整任务信息</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<script type="text/javascript"> 
	$(document).ready(function(e) {
		$(".select1").uedSelect({
			width : 118			  
		});
	});
	</script>

    
</head>

<body>

<form action="TaskServlet?method=updateTask&target=taskadjust&task_state1=0" method="post">
<div class="MainContent">

	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">任务调整</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>
	
	<div class="mainindex"> 
	
		<%@ include file="/WEB-INF/include/detailedtask.jsp"%>
		
		<div class="buttonArea">
			<input type="button" value="重置" class="InfoButton2" onclick="this.form.reset();return false;"/>
			<input type="submit" value="提交" class="InfoButton2"/>
		</div>	
	
  </div> 

</div>

</form>
</body>
</html>
