<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>跟踪任务</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   
</head>

<body>

<div class="MainContent">

	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">任务跟踪</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>
	
	<div class="mainindex">
	

		<%@ include file="/WEB-INF/include/tasklist.jsp"%>
   
	    <div class="buttonArea">
	   		<input id="taskbutton" name="" type="button" value="详细信息" class="InfoButton"/>
	    </div>
	   <script type="text/javascript">
	   		$("#taskbutton").click(function(){
	   			var taskId=$(":input:radio:checked").val();
	   			if(taskId !=null && taskId!= ""){
	   				window.location="TaskServlet?method=detailedtaskinformation&taskId="
	   					+taskId+"&target=tasktrackinformation";
	   			}else{
	   				alert("请选择信息");
	   			}
	   			
	   		});
	   </script>
	   
	   
	   
 </div>


</div>


</body>
</html>
