<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>跟踪计划信息</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  
	<script type="text/javascript"> 
	$(document).ready(function(e) {
		$(".select1").uedSelect({
			width : 118			  
		});
	});
	</script>
	<link type="text/css" rel="stylesheet" href="../css/select.css"/>
    <link type="text/css" rel="stylesheet" href="../css/pageCommon.css"/>
</head>

<body>

<div class="MainContent">

	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">任务详情</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>
	
	<div class="mainindex"> 
	
	<form action="TaskServlet?method=updateTask&target=tasktrack&task_state1=1" method="post">
		
		<%@ include file="/WEB-INF/include/detailedtask.jsp"%>
		
		
		<div class="buttonArea"><input name="" type="submit" value="提交" class="InfoButton2" /></div>	
	</form>
		<div class="MainNullArea"></div>

		<div class="MainTitle2">计划信息 </div>
		
		
		
		<%@ include file="/WEB-INF/include/feedbacklist.jsp"%>
		
		
		
		<div class="buttonArea">
		<input type="hidden" id="flag" name=""/>
		<input name="" type="button" value="返回" class="InfoButton2" onclick="javascript:history.go(-1);"/>
		<input id="feedbackbutton" type="button" onclick="" value="反馈信息" name="" class="InfoButton" />
		<script type="text/javascript">
			$("#feedbackbutton").click(function(e){
				var checkedfeedback=$(":input:checkbox:checked");
				var td=$(".tdfeedback");
				flag=$("#flag");
				if(flag.attr("name") != "1"){
					td.removeAttr("style")
					td.attr("style","background-color:#FFFF00"); 
					flag.attr("name","1"); 
				//	alert(flag.attr("name"));
					//td.html(feedback);
				}else{
					td.attr("style","display:none");
					flag.removeAttr("name")
				}
				
			});
		
		
		</script>
		</div>
	</div> 
 
</div>


</body>
</html>
