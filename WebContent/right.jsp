<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<%@ include file="/WEB-INF/include/base.jsp"%>
    <title>Right</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link type="text/css" rel="stylesheet" href="css/pageCommon.css"/>
</head>
<body  style="margin:0">
<div class="MainContent">
	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">首 页</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>
	<c:choose>
		<c:when test="${empty loginUser.username }">
			
			<div class="mainindex">
				<div class="welinfo">
				<span><img src="images/sun.png" alt="天气" /></span>
				<b>您还未登录，请点击登录  </b><a href="login.jsp" target="_parent">
	            		  登录</a></div>
				
				
	            		
				
				
			</div>	
		
		
		</c:when>
		<c:otherwise>
				
			<div class="mainindex">
				<div class="welinfo">
				<span><img src="images/sun.png" alt="天气" /></span>
				<b>${loginUser.username } 早上好，欢迎使大唐电信管理服务系统</b><a href="#" target="_parent">帐号设置</a>		</div>
				
				<div class="welinfo">
				<span><img src="images/time.png" alt="时间" /></span>
				<i>您本次登录的时间：${date }</i> （不是您登录的？<a href="#" target="_parent">请点这里</a>）		</div>
				<div class="xline"></div>
				
			</div>
		
		</c:otherwise>
	
	</c:choose>
	
	
</div>

</body>
</html>