<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript"> 
	$(document).ready(function(e) {
		$(".select1").uedSelect({
			width : 118			  
		});
	});
	</script>
</head>
<body>

<div  class="ItemBlock">
		<table border="0"cellspacing="1" cellpadding="1" class="dateTable" >
		
			<tr>
				<th width="12%">任务名称</th>
				<td colspan="3">
					<input id="taskid" name="id" type="hidden" value="${task.id }">
					<input name="task_name" type="text" class="txtInput" value="${task.task_name }"/>
				</td>
			</tr>
			<tr>
				<th >任务描述</th>
				<td colspan="3"><textarea name="task_describe" class="TxtArea">${task.task_describe }</textarea></td>
			</tr>
			<tr>
				<th>开始时间</th>
			  <td width="33%"><input name="task_begin_time1" type="date" class="txtInput2" value="${task.task_begin_time }"/></td>
				<th width="12%" >结束时间</th>
			  <td width="43%"><input name="task_end_time1" type="date" class="txtInput2" value="${task.task_end_time }"/></td>
			</tr>
			<tr>
				<th>实施人</th>
				
				<%-- <td><input name="user_id" type="text" class="txtInput2" value="${user.username }"/></td> --%>
		
				<td>
					<select name="user_id" class="select1">
						<c:forEach items="${userList }" var="user">
							<option value="${user.id } " 
								<c:if test="${user.id eq task_user.id }">
									selected="selected"
								</c:if> >
								${user.username }
							</option>
						</c:forEach>
						
					</select>
				</td>
				
				<th>任务状态</th>
				<td>
					<select  name="task_state" class="select1"  >
						<option value="0" <c:if test="${task.task_state eq '0' }">selected="selected"</c:if>>未实施</option>
						<option value="1" <c:if test="${task.task_state eq '1' }">selected="selected"</c:if>>实施中</option>
						<option value="2" <c:if test="${task.task_state eq '2' }">selected="selected"</c:if>>已完成</option>
					</select>
					
				</td>
			</tr>
		</table>
		</div>

</body>
</html>