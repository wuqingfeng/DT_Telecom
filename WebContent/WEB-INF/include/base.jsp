<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<base href="http://${pageContext.request.serverName }:${pageContext.request.serverPort}${pageContext.request.contextPath }/"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript"  src="js/jquery.js"></script>
<script language="javascript" src="js/pageCommon.js" charset="utf-8"></script>
<script language="javascript" src="js/PageUtils.js" charset="utf-8"></script>
<script language="javascript" src="js/DemoData.js" charset="utf-8"></script>
<script language="javascript" src="js/DataShowManager.js" charset="utf-8"></script>
<script language="JavaScript" src="js/select-ui.min.js"></script>
<script language="JavaScript" src="js/Calendar3.js"></script>
<link type="text/css" rel="stylesheet" href="css/pageCommon.css"/>
<link type="text/css" rel="stylesheet" href="css/select.css"/>



<script type="text/javascript" src="js/cloud.js" ></script>

