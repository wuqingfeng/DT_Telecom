<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript"> 
	$(document).ready(function(e) {
		$(".select1").uedSelect({
			width : 118			  
		});
	});
	</script>

</head>
<body>

     <div class="ItemBlock">
	  <table border="0"cellspacing="1" cellpadding="1" class="dateTable" >
        <tr>
          <th >计划名称</th>
          
          <td colspan="3">
          <input name="id" value="${plan.id }" type="hidden">
          <input name="plan_name" type="text" class="txtInput" value="${plan.plan_name }" readonly="ture"/> </td>
       </tr>
       <tr>
          <th>计划描述</th>
          <td colspan="3"><textarea name="plan_describe" class="TxtArea" readonly="readonly">${plan.plan_describe }</textarea></td>
       </tr>
        <tr>
          <th >开始时间</th>
          <td><input name="plan_begin_time1" type="date" class="txtInput2" value="${plan.plan_begin_time }" readonly="ture"/></td>
          <th >结束时间</th>
          <td ><input name="plan_end_time1" type="date" class="txtInput2" value="${plan.plan_end_time }" readonly="ture"/></td>
        </tr>

        <tr>
          <th>任务状态</th>
          <td >
          		<input type="hidden" name="task_id" value="${plan.task_id }">
				<select   class="select1" disabled="disabled">
					<option value="0"  <c:if test="${task.task_state  eq '0' }">selected="selected"</c:if>>未实施</option>
					<option value="1"  <c:if test="${task.task_state  eq '1' }">selected="selected"</c:if>>实施中</option>
					<option value="2"  <c:if test="${task.task_state  eq '2' }">selected="selected"</c:if>>已完成</option>
				</select>
		  </td>
         <th>计划状态</th>
         <td >
         	<!-- 当反馈被提交后  默认将反馈状态变为已反馈 -->
         	<input type="hidden" name="plan_state" value="1">
			<select name=""   class="select1"  disabled="disabled">
				<option value="0" <c:if test="${plan.plan_state eq '0' }">selected="selected"</c:if>>未反馈</option>
				<option value="1" <c:if test="${plan.plan_state eq '1' }">selected="selected"</c:if>>已反馈</option>
			</select>
		  </td>
       </tr>
	   <tr>
          <th>反馈信息</th>
          <td colspan="3"><textarea name="feedback" class="TxtArea" >${plan.feedback }</textarea></td>
       </tr>
    </table>
	</div>

</body>
</html>