<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"> 
	$(document).ready(function(e) {
		$(".select1").uedSelect({
			width : 118			  
		});
	});
	</script>
</head>
<body>

<table cellspacing="0" cellpadding="0" class="tablelist">
		
		<!-- 表头-->
		<thead>
			<tr>
				<th width="38%">计划名称</th>
				<th width="14%">任务状态</th>
				<th width="12%">是否反馈</th>
				<th width="15%">开始时间</th>
				<th width="15%">结束时间</th>
			  <th width="6%"><input id="checktop" name="" type="checkbox" value=""></th>
			</tr>
		</thead>
		
		<!--显示数据列表-->
		<tbody id="TableData" >
		<c:forEach items="${planList }" var="plan">
		   <tr class="TableDetail template">
              <td>${plan.plan_name}&nbsp;</td>
              
              <td>
				<c:choose>
					<c:when test="${task.task_state eq '0' }">未实施</c:when>
					<c:when test="${task.task_state eq '1' }">实施中</c:when>
					<c:when test="${task.task_state eq '2' }">已完成</c:when>
					<c:otherwise>&nbsp;</c:otherwise>
				</c:choose>
			</td>
              
              <td>	
	              <c:choose>
						<c:when test="${plan.plan_state eq '0' }">未反馈</c:when>
						<c:when test="${plan.plan_state eq '1' }">已反馈</c:when>
						<c:otherwise>&nbsp;</c:otherwise>
				 </c:choose>
              </td>
              <td>${plan.plan_begin_time}&nbsp;</td>
              <td>${plan.plan_end_time}&nbsp;</td>
              <td ><input id="feedback" type="checkbox" name="${plan.id }"  /></td>
            </tr>
            <tr  class="TableDetail template">
				 <td class="tdfeedback" colspan="6" style="background-color:#FFFF00;display:none">反馈信息：&nbsp;   ${plan.feedback }&nbsp;</td>
			</tr>
		</c:forEach>   

			 
		</tbody>
		</table>
		
		<script type="text/javascript">
			
			
			$("#checktop").change(function(){
				if($(this).is(":checked")){
					$(":checkbox").attr("checked","checked");
				}else{
					$(":checkbox").removeAttr("checked");
				}
			});
		
		
		</script>

</body>
</html>