<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>


					<c:forEach items="${userList }" var="user">
						<!--  class="dataContainer" -->
						<tr class="TableDetail template">
							<td>${user.username}&nbsp;</td>

							<c:choose>
								<c:when test="${user.gender eq '0'}">
									<td align="center">男&nbsp;</td>
								</c:when>
								<c:when test="${user.gender eq '1'}">
									<td align="center">女&nbsp;</td>
								</c:when>
								<c:otherwise>
									<td align="center">&nbsp;</td>
								</c:otherwise>
							</c:choose>
							<td>${user.birthday}&nbsp;</td>
							<td>${user.hiredate}&nbsp;</td>
							<td>${user.duty}&nbsp;</td>
							<c:choose>
								<c:when test="${user.role eq '0'}">
									<td>系统管理员&nbsp;</td>
								</c:when>
								<c:when test="${user.role eq '1'}">
									<td>高级主管&nbsp;</td>
								</c:when>
								<c:when test="${user.role eq '2'}">
									<td>普通员工&nbsp;</td>
								</c:when>
								<c:otherwise>
									<td>&nbsp;</td>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${!empty user.superior_id }">
									<c:forEach items="${userList }" var="userSuperior">
										<c:if test="${userSuperior.id eq user.superior_id }">
											<c:set value="${userSuperior.username}" var="supername"></c:set>
											
											<td >${userSuperior.username}&nbsp;</td>
										</c:if>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<td>&nbsp;</td>
								</c:otherwise>
							</c:choose>
							<td>${user.password}&nbsp;</td>
							<td><input class="usercheck" name="${supername }"
								type="checkbox" value="${user.id }" /></td>
						</tr>
						</c:forEach>
				
				

</body>
</html>