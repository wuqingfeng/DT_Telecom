<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/include/base.jsp"%>
</head>
<body>
	<table cellspacing="0" cellpadding="0" class="tablelist">      
        <!-- 表头-->
       <thead>
			<tr>
				<th width="38%">任务名称</th>
				<th width="16%">实施人</th>
				<th width="14%">开始时间</th>
				<th width="14%">结束时间</th>
				<th width="12%">任务状态</th>
				<th width="6%"></th>
			</tr>
        </thead>

		<!--显示数据列表-->
		
        <tbody id="TableData" >
        <c:forEach items="${page.data }"  var="task">
			<tr class="TableDetail template">
			<%-- <a href="TaskServlet?method=detailedtaskinformation&taskId=${task.id }&target=detailedtaskinformation">${task.task_name}</a> --%>
				<td>${task.task_name}&nbsp;</td>
				
			 	<c:if test="${!empty userList }">
					<c:forEach items="${userList }" var="user">
						<c:if test="${user.id eq task.user_id}">
						<td><a href="TaskServlet?method=workerInfomation&userId=${user.id }">${user.username }</a>&nbsp;</td>
						</c:if>
					</c:forEach>
				 </c:if>

				 <c:if test="${!empty userSuperior}">
					<td>${userSuperior.username}&nbsp;</td>
				</c:if> 
				
				
				
				<td>${task.task_begin_time}&nbsp;</td>
				<td>${task.task_end_time}&nbsp;</td>
				<c:choose>
					<c:when test="${task.task_state eq '0'}">
						<td>未实施&nbsp;</td>
					</c:when>
					<c:when test="${task.task_state eq '1'}">
						<td>实施中&nbsp;</td>
					</c:when>
					<c:when test="${task.task_state eq '2'}">
						<td>已完成&nbsp;</td>
					</c:when>
					<c:otherwise>
						<td>&nbsp;</td>
					</c:otherwise>
				</c:choose>
				
				
				<td><input  type="radio" name="radiobutton" value="${task.id }" /></td>
		
			</tr>
		</c:forEach>
        </tbody>
   </table>
   <div align="center"><%@include file="/WEB-INF/include/page.jsp" %></div>
   
</body>
</html>