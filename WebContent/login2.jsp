﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<%@ include file="/WEB-INF/include/base.jsp"%>

<title>欢迎登录后台管理系统</title>

<script type="text/javascript">
$(document).ready(function(e) {
    $(".select3").uedSelect({
		width : 128			  
	});
});
</script>
<script language="javascript">
	$(function(){
    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
	$(window).resize(function(){  
    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
    })  
});  
</script> 

</head>

<body>

<div id="mainBody">
      <div id="cloud1" class="cloud"></div>
      <div id="cloud2" class="cloud"></div>
</div>  


<div class="logintop">    
    <span>欢迎登录后台管理界面平台</span>    
    <ul>
    <li><a href="#">回首页</a></li>
    <li><a href="#">帮助</a></li>
    <li><a href="#">关于</a></li>
    </ul>    
</div>
    
<div class="loginbody">
    
	<div class="systemlogo"></div>
       
    <div class="loginbox">
    <form action="UserServlet?method=login" method="post">
		<ul>
			<li>
			
			<select name="role"  class="select3">
				<option value="0">系统管理员</option>
				<option value="1">主管</option>
				<option value="2">普通员工</option>
			</select>
			<span style="color:red">${msg }</span>
			
			<c:forEach items="${pageContext.request.cookies }" var="c">
				<c:if test="${c.name eq 'userinfo' }">
					
				</c:if>
				<c:choose>
					<c:when test="${c.name eq 'userinfo' }">
						<c:set value="${fn:split(c.value,'-')}" var="info"></c:set>
						<c:set value="${info[0] }" var="username"></c:set>
						<c:set value="${info[1] }" var="password"></c:set>
					</c:when>
					<c:otherwise>
						<c:set value="请输入用户名" var="username"></c:set>
						<c:set value="请输入密码" var="password"></c:set>
					</c:otherwise>
				
				</c:choose>
			</c:forEach>
			</li>
			<li><input name="username" type="text" class="loginuser" value="${username }" onclick="JavaScript:this.value=''"/></li>
			<li><input name="password" type="text" class="loginpwd" value="${password }" onclick="JavaScript:this.value=''"/></li>
			<li><input name="" type="submit" class="loginbtn" value="登录"   />
			<label><input name="ck" type="checkbox"  checked="checked" />记住密码</label><label><a href="#">忘记密码？</a></label>
			</li>
		</ul>
	</form>
	
    </div>
</div>    
       
<div class="loginbm">版权所有  2014  <a href="">东方标准</a> </div>	
    
</body>
</html>
