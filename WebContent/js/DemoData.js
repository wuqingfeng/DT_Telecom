// JavaScript Document
var demoData = {
	taskInformationList:[
		{"taskInfor.taskname":"任务一","taskInfor.worker":"张三","taskInfor.begintime":"2013-03-13","taskInfor.endtime":"2013-06-23","taskInfor.condition":"实施中"},
		{"taskInfor.taskname":"任务二","taskInfor.worker":"李四","taskInfor.begintime":"2013-03-15","taskInfor.endtime":"2013-07-23","taskInfor.condition":"未实施"},
		{"taskInfor.taskname":"任务三","taskInfor.worker":"王五","taskInfor.begintime":"2013-04-13","taskInfor.endtime":"2013-08-23","taskInfor.condition":"已完成"}
	],
	taskInformation2List:[
		{"taskInfo2.taskname":"任务一","taskInfo2.worker":"张三","taskInfo2.begintime":"2013-03-13","taskInfo2.endtime":"2013-06-23","taskInfo2.condition":"未实施"},
		{"taskInfo2.taskname":"任务二","taskInfo2.worker":"李四","taskInfo2.begintime":"2013-03-15","taskInfo2.endtime":"2013-07-23","taskInfo2.condition":"未实施"},
		{"taskInfo2.taskname":"任务三","taskInfo2.worker":"张三","taskInfo2.begintime":"2013-04-13","taskInfo2.endtime":"2013-08-23","taskInfo2.condition":"未实施"}
	],
	taskadjustinformationList:[
		{"taskadjustinfo.taskname":"任务一","taskadjustinfo.taskdescribe":"任务描述一………………","taskadjustinfo.begintime":"2013-03-13","taskadjustinfo.endtime":"2013-06-23","taskadjustinfo.realbegintime":"2013-03-13","taskadjustinfo.realendtime":"2013-06-30","taskadjustinfo.worker":"赵六","taskadjustinfo.condition":"未实施"}
	],
	detailedtaskInforList:[
		{"detailedtaskInfor.taskname":"任务一","detailedtaskInfor.taskdescribe":"任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一任务描述一","detailedtaskInfor.begintime":"2013-03-13","detailedtaskInfor.endtime":"2013-06-23","detailedtaskInfor.realbegintime":"2013-03-13","detailedtaskInfor.realendtime":"2013-06-30","detailedtaskInfor.worker":"赵七","detailedtaskInfor.condition":"实施中"}
	],
	implementingtaskList:[
		{"taskInfor.taskname":"任务一","taskInfor.worker":"张三","taskInfor.begintime":"2013-03-13","taskInfor.endtime":"2013-06-23","taskInfor.condition":"实施中"},
		{"taskInfor.taskname":"任务二","taskInfor.worker":"李四","taskInfor.begintime":"2013-03-15","taskInfor.endtime":"2013-07-23","taskInfor.condition":"实施中"},
		{"taskInfor.taskname":"任务三","taskInfor.worker":"王五","taskInfor.begintime":"2013-04-13","taskInfor.endtime":"2013-08-23","taskInfor.condition":"实施中"}
	],
	implementingtask2List:[
		{"taskInfor.taskname":"任务一","taskInfor.taskdescribe":"任务描述……","taskInfor.begintime":"2013-03-13","taskInfor.endtime":"2013-06-23","taskInfor.worker":"张三","taskInfor.condition":"实施中"}
	],
	planList:[
	    {"plan.name":"计划一","plan.status":"已完成","plan.feedback":"已反馈","plan.begintime":"2013-03-13","plan.endtime":"2013-06-23"},
		{"plan.name":"计划二","plan.status":"已完成","plan.feedback":"已反馈","plan.begintime":"2013-03-13","plan.endtime":"2013-04-23"},
		{"plan.name":"计划三","plan.status":"已完成","plan.feedback":"已反馈","plan.begintime":"2013-04-13","plan.endtime":"2013-05-13"}
    ],
	plan2List:[
		{"plan.name":"计划一","plan.status":"已完成","plan.feedback":"已反馈","plan.begintime":"2013-03-13","plan.endtime":"2013-06-23","plan.feedbackInfo":"该计划已经顺利完成……"},
		{"plan.name":"计划二","plan.status":"已完成","plan.feedback":"未反馈","plan.begintime":"2013-03-13","plan.endtime":"2013-04-23","plan.feedbackInfo":"该计划已经顺利完成……"}
    ],
	plandetailInformationList:[
	    {"plandetailInfo.name":"计划一","plandetailInfo.describe":"计划描述……","plandetailInfo.begintime":"2013-03-13","plandetailInfo.endtime":"2013-06-23","plandetailInfo.taskname":"任务一","plandetailInfo.status":"未完成",
"plandetailInfo.backInfo":"反馈信息……反馈信息……反馈信息"}
	],
	mytaskList:[
	  {"mytask.taskname":"任务一","mytask.leader":"主管一","mytask.begintime":"2013-03-13","mytask.endtime":"2013-04-23","mytask.status":"实施中"},
	  {"mytask.taskname":"任务二","mytask.leader":"主管一","mytask.begintime":"2013-04-13","mytask.endtime":"2013-06-23","mytask.status":"未实施"},
	  {"mytask.taskname":"任务三","mytask.leader":"主管一","mytask.begintime":"2013-04-19","mytask.endtime":"2013-08-23","mytask.status":"未实施"}
	],
	myplanList:[
	    {"myplan.name":"计划一","myplan.status":"已完成","myplan.feedback":"已反馈","myplan.begintime":"2013-03-13","myplan.endtime":"2013-06-23"},
		{"myplan.name":"计划二","myplan.status":"已完成","myplan.feedback":"未反馈","myplan.begintime":"2013-03-13","myplan.endtime":"2013-04-23"}
    ],
	myplanfeedbackList:[
	    {"myplan.name":"计划一","myplan.describe":"计划描述……","myplan.status":"已完成","myplan.feedback":"未反馈","myplan.begintime":"2013-03-13","myplan.endtime":"2013-06-23"}
    ],
	inquriemyplanList:[
	    {"myplan.name":"计划一","myplan.taskname":"任务一","myplan.status":"未实施","myplan.feedback":"已反馈","myplan.begintime":"2013-03-13","myplan.endtime":"2013-06-23"},
		{"myplan.name":"计划二","myplan.taskname":"任务一","myplan.status":"未实施","myplan.feedback":"未反馈","myplan.begintime":"2013-03-13","myplan.endtime":"2013-04-23"}
    ],
	workerList:[
	   {"worker.name":"张三","worker.sex":"男","worker.beginworktime":"2000-01-01","worker.post":"渠道专员"},
	   {"worker.name":"李生","worker.sex":"男","worker.beginworktime":"2000-01-01","worker.post":"市场专员"},
	   {"worker.name":"王六","worker.sex":"男","worker.beginworktime":"2000-01-01","worker.post":"渠道专员"},
	   {"worker.name":"赵七","worker.sex":"男","worker.beginworktime":"2000-01-01","worker.post":"市场专员"},
	   {"worker.name":"五一","worker.sex":"男","worker.beginworktime":"2000-01-01","worker.post":"渠道专员"}
	],
	workerInformationList:[
	   {"worker.name":"张三","worker.sex":"男","worker.birthtime":"1998-12-25","worker.degree":"本科","worker.discipline":"销售管理","worker.beginworktime":"2000-01-01","worker.post":"渠道专员","worker.workexperiences":"行业经历……"}
	],
	userList:[
	  {"user.name":"张三","user.sex":"男","user.birthtime":"1998-12-25","user.beginworktime":"2000-01-01","user.post":"系统管理员","user.postdescribe":"该职位主要负责……该职位的主要工作是……","user.role":"系统管理员","user.leader":"","user.InitialPw":"123456"},
	  {"user.name":"李四","user.sex":"男","user.birthtime":"1998-12-25","user.beginworktime":"2000-01-01","user.post":"部门经理","user.postdescribe":"该职位主要负责……","user.role":"高级主管","user.leader":"","user.InitialPw":"123456"},
	  {"user.name":"赵六","user.sex":"男","user.birthtime":"1998-12-25","user.beginworktime":"2000-01-01","user.post":"部门经理","user.postdescribe":"该职位主要负责……","user.role":"普通员工","user.leader":"李四","user.InitialPw":"123456"},
	  {"user.name":"王五","user.sex":"男","user.birthtime":"1998-12-25","user.beginworktime":"2000-01-01","user.post":"市场专员","user.postdescribe":"该职位主要负责……","user.role":"普通员工","user.leader":"","user.InitialPw":"123456"}
	],
	userdeleteList:[
	  {"user.name":"赵七","user.sex":"男","user.birthtime":"1998-12-25","user.beginworktime":"2000-01-01","user.postdescribe":"总经理员 该职位主要负责……","user.role":"高级主管"},
	  {"user.name":"李四","user.sex":"男","user.birthtime":"1998-12-25","user.beginworktime":"2000-01-01","user.postdescribe":"部门经理 该职位主要负责……","user.role":"中级主管"},
	  {"user.name":"王五","user.sex":"男","user.birthtime":"1998-12-25","user.beginworktime":"2000-01-01","user.postdescribe":"市场专员 该职位主要负责……","user.role":"普通员工"}
	],
	distributionworkerList:[
	  {"worker.name":"赵七","worker.sex":"男","worker.birthtime":"1998-12-25","worker.beginworktime":"2000-01-01","worker.postdescribe":"部门经理 该职位主要负责……","worker.role":"高级主管"},
	  {"worker.name":"李四","worker.sex":"男","worker.birthtime":"1998-12-25","worker.beginworktime":"2000-01-01","worker.postdescribe":"销售人员 该职位主要负责……","worker.role":"普通员工"},
	  {"worker.name":"王五","worker.sex":"男","worker.birthtime":"1998-12-25","worker.beginworktime":"2000-01-01","worker.postdescribe":"市场专员 该职位主要负责……","worker.role":"普通员工"}
	],
	distributionworkerInformationList:[
	   {"worker.name":"李四","worker.sex":"男","worker.birthtime":"1998-12-25","worker.degree":"本科","worker.discipline":"销售管理","worker.beginworktime":"2000-01-01","worker.role":"普通员工","worker.leader":"","worker.post":"市场专员","worker.postdescribe":"该职位主要负责……"}
	],
    xxList: []
};

// list的最后一个元素后不要有逗号，否则在页面中显示数据时，会多出一行模板行。是因为没有数据内容造成的。
// （是使用替换的方式完成的显示数据，如果没有替换值，就不会替换，所以会多一个模板行）
