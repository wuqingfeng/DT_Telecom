<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>导航菜单</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="JavaScript" src="js/jquery.js"></script>
<script language="JavaScript" src="js/menu.js"></script>
<link type="text/css" rel="stylesheet" href="css/style.css"/>
<script type="text/javascript">
	var basePath = "";
	var SubImg = basePath + 'img/MenuIcon-1.jpg';
	var SubImgOpen = basePath + 'img/MenuIcon-2.jpg';
	$(function(){
	   var $level1=$(".level1 .level1Style");
	   var $showlevel1=$(".level1 .MenuLevel2");
	   $level1.each(function(index){
	   $(this).click(function(){
		 if(!$(this).is(":animated")){
		
		  $showlevel1.eq(index+1).slideToggle("normal").end()
		  .not($showlevel1[index+1]).slideUp("normal");  
			};
		  });
		
	  });
	});
</script>
<script type="text/javascript">
$(function(){	
	//导航切换
	$(".level1Style").click(function(){
		$(".level1Style.selected").removeClass("selected")
		$(this).addClass("selected");
	})	
})
$(function(){	
	//导航切换
	$(".level2Style").click(function(){
		$(".level2Style.selected").removeClass("selected")
		$(this).addClass("selected");
	})	
})		
</script>

</head>

<body style="margin: 0; ">
<div id="Menu">
    <ul class="MenuUl">
        <li class="level1">
			<div onClick="menuClick(this);"  class="level1Sty"></div>
			<ul style="display: none;"  class="MenuLevel2">	</ul>
		</li>
		<c:if test="${empty loginUser }">
			 <li class="level1">
	            <a style="color:#FFFFFF" href="login.jsp" target="_parent">
	            		<div class="level1Style">任务管理 </div></a>
	         </li>
	         <li class="level1">
	            <a style="color:#FFFFFF" href="login.jsp" target="_parent">
	            		<div class="level1Style">计划管理 </div></a>
	         </li>
	         <li class="level1">
	            <a style="color:#FFFFFF" href="login.jsp" target="_parent">
	            		<div class="level1Style">人员管理 </div></a>
	         </li>
	         
	         
		</c:if>
		<c:choose >
			<c:when test="${loginUser.role eq '1' }">
			
			<li class="level1">
	            <div class="level1Style">任务管理</div>
	            <ul style="display: none;" class="MenuLevel2" >
	                <li class="level2">
	                    <div class="level2Style" onclick="parent.rightFrame.location.href='TaskServlet?method=showTask&target=taskinformation&task_state=0,1,2'">查看任务</div>
	                </li>
	                <li class="level2">
	                    <div class="level2Style" onclick="parent.rightFrame.location.href='TaskServlet?method=createTask'">制定任务</div>
	                </li>
	                <li class="level2">
	                    <div class="level2Style" onclick="parent.rightFrame.location.href='TaskServlet?method=showTask&target=tasktrack&task_state=1'">跟踪任务</div>
	                </li>
	                <li class="level2">
	                    <div class="level2Style" onclick="parent.rightFrame.location.href='TaskServlet?method=showTask&target=taskadjust&task_state=0'">调整任务</div>
	                </li>
	                <li class="level2">
	                	<div class="level2Style" onclick="parent.rightFrame.location.href='TaskServlet?method=findUser'">查看人员</div>
	                </li>
	            </ul>
	        </li>
			
			
			</c:when>
			<c:when test="${loginUser.role eq '2' }">
			
		  <li class="level1">
            <div class="level1Style">计划管理</div>
            <ul style="display: none;" class="MenuLevel2">
                <li class="level2">
                	<div class="level2Style" onclick="parent.rightFrame.location.href='PlanServlet?method=findTask'">我的任务</div>
                </li>
<!--				
                <li class="level2">
                    <div class="level2Style"><img src="img/menuimages/xtb4.jpg" class="Icon2"><a target="right" >制定计划</a></div>
                </li>
                <li class="level2">
                    <div class="level2Style"><img src="img/menuimages/xtb4.jpg" class="Icon2"><a target="right" >反馈计划</a></div>
                </li>
                <li class="level2">
                    <div class="level2Style"><img src="img/menuimages/xtb4.jpg" class="Icon2"><a target="right">调整计划</a></div>
                </li>
-->	
			
                <li class="level2">
                    <div class="level2Style" onclick="parent.rightFrame.location.href='PlanServlet?method=findTaskList'">查询计划</div>
                </li>
            </ul>
        </li>	
			
			
			
			
			</c:when>
			<c:when test="${loginUser.role eq '0' }">
			
			
				

        <li class="level1">
            <div class="level1Style" >人员管理</div>
      
		    <ul style="display: none;" class="MenuLevel2">
                <li class="level2">
                    <div class="level2Style" onclick="parent.rightFrame.location.href='ManagerServlet?method=userList'">人员管理</div>
                </li>
			   <!--  	
                <li class="level2">
                    <div class="level2Style" onclick="parent.rightFrame.location.href='person/deleteperson.html'">删除人员</div>
                </li>
                <li class="level2">
                    <div class="level2Style" onclick="parent.rightFrame.location.href='person/distributionworker.html'">分配人员</div>
                </li>
			-->	
            </ul>
			
        </li>
			
			</c:when>

		</c:choose>
	    
	

		
    </ul>
</div>
</body>
</html>
