<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>查询计划</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<script type="text/javascript"> 
	$(document).ready(function(e) {
		$(".select1").uedSelect({
			width : 118			  
		});
	});
	</script>
</head>

<body>



<div class="MainContent">

	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">计划查询</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>
	
	<div class="mainindex">
	<div class="ItemBlock">
	<form action="PlanServlet?method=findPlan" method="post">
     <table border="0" cellspacing="0" cellpadding="0" class="dateTable">
        <tr>
          <th width="12%">计划名称</th>
          <td><input name="plan_name" type="text" class="txtInput4" value=""/></td>


       </tr>
       <tr>
			<th >所属任务</th>
			<td width="64%" >
				<select class="select1" name="task_id" >
					<option value="-1" selected="selected"  >所属任务</option>
				<c:forEach items="${listTask }" var="task">
					<option value="${task.id }">${task.task_name }</option>
				</c:forEach>
					
				</select>
	      </td>
		</tr>
		<tr>
		

			<th>开始时间</th>
			<td>
			从　<input type="date" name="bbegintime" class="txtInput3" />　
			到　<input type="date" name="bendtime" class="txtInput3"/>
			</td>
       </tr>
       <tr>
			<th>结束时间</th>
			<td>
			从　<input type="date" name="ebegintime" class="txtInput3"   />　
			到　<input type="date" name="eendtime" class="txtInput3"/>
			</td>
		</tr> 
		 
		<tr>
			<th width="12%">是否反馈</th>
			<td width="64%" >
				<select class="select1" name="plan_state">
					<option value="" >是否反馈？</option>
					<option value="0">未反馈</option>
					<option value="1">已反馈</option>
				</select>
		  </td>
		  
		  <td width="24%" rowspan="5" style="text-align:center">
		  <input name="" type="submit" value="查询" class="InfoButton3"/>
		  </td>
        </tr> 
    </table>
    </form>
	</div>
  

     <div class="MainNullArea"></div>
	 <div class="MainTitle2">查询结果</div>
	 
<c:choose>
   <c:when test="${empty listPlan }">
        		<h1>未查询到数据</h1>
   </c:when>
   <c:otherwise>
   

	 <table cellspacing="0" cellpadding="0" class="tablelist">     
       <thead>
            <tr align="center" valign="middle" id="TableTitle">
            	<th width="26%">计划名称</th>
				
				<th width="14%">开始时间</th>
				<th width="14%">结束时间</th>
				<th width="26%">所属任务</th>
				<th width="10%">任务状态</th>
				<th width="10%">是否反馈</th>
            </tr>
        </thead>

        <tbody>
        <c:forEach items="${listPlan }" var="plan">
			<tr>
					<td>${plan.plan_name }</td>	
					<td>${plan.plan_begin_time }</td>
					<td>${plan.plan_end_time }</td>
				<c:choose>
					<c:when test="${empty listTask }">
						<td></td>
						<td></td>
					</c:when>
					<c:otherwise>
						<c:forEach items="${listTask }" var="task">
							<c:if test="${plan.task_id eq task.id }">
									<td>${task.task_name }</td>
									<td>
										<c:choose>
											<c:when test="${task.task_state eq '0' }">未实施</c:when>
											<c:when test="${task.task_state eq '1' }">实施中</c:when>
											<c:when test="${task.task_state eq '2' }">已完成</c:when>
											<c:otherwise></c:otherwise>
										</c:choose>
									</td>
							</c:if>
						</c:forEach>
					</c:otherwise>
				</c:choose>
					
					<td>
						<c:choose>
							<c:when test="${plan.plan_state eq 0 }">未反馈</c:when>
							<c:when test="${plan.plan_state eq 1 }">已反馈</c:when>
							<c:otherwise></c:otherwise>
						</c:choose>
					</td>
			</tr>
		</c:forEach>
        </tbody>
   </table>
    
   </c:otherwise>  
</c:choose>   
   </div>

</div>


</body>
</html>
