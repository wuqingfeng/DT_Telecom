<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>新建计划</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


</head>

<body>
<form action="PlanServlet?method=savePlan" method="post"> 
<div class="MainContent">

	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">新建计划</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>
	
	<div class="mainindex">
		<div class="ItemBlock">
		<table border="0" cellspacing="0" cellpadding="0"class="dateTable">
			<tr>
				<th width="12%" >计划名称</th>
				<td colspan="3"><input type="text" name="plan_name" class="txtInput"/></td>
			</tr>

			
			
			<tr>
				<th>计划描述</th>
				<td colspan="3"><textarea name="plan_describe" class="TxtArea"></textarea></td>
			</tr>
			<tr>
				<th >开始时间</th>
			  <td width="33%"><input type="date" name="plan_begin_time1" class="txtInput2"/></td>
				<th width="12%" >结束时间</th>
			  <td width="43%" ><input type="date" name="plan_end_time1"  class="txtInput2"/></td>
			</tr>
			<tr>
				<th>计划状态</th>	
			<td >
          		<input type="hidden" name="task_id" value="${task.id }">
				<c:choose>
					<c:when test="${task.task_state eq '0' }">
						<input type="text"  value="未实施" class="txtInput2" disabled="disabled"></c:when>
					<c:when test="${task.task_state eq '1' }">
						<input type="text"  value="实施中" class="txtInput2" disabled="disabled"></c:when>
					<c:when test="${task.task_state eq '2' }">
						<input type="text"  value="已完成" class="txtInput2" disabled="disabled"></c:when>
					<c:otherwise><input type="text"  value="" class="txtInput2" disabled="disabled"></c:otherwise>
				</c:choose>
		  </td>
		  
				<th>是否反馈</th>
				<td>
				<!-- 新建计划 默认为未反馈 -->
				<input type="hidden" name="plan_state" value="0"/>
				<input type="text" class="txtInput2"  value="未反馈" disabled="disabled"/></td>
			</tr>
		</table>
		</div>
		
		<div class="buttonArea">	   
		   <input name="" type="button" value="重置" class="InfoButton2" onclick="this.form.reset();return false;"/>
		   <input name="" type="submit" value="提交" class="InfoButton2" onclick="javascript:history.go(-1);"/>
	   </div>	
	</div> 
  

</div>

</form>
</body>
</html>
