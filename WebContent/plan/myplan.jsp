<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>

<title>制定计划</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	
	<link type="text/css" rel="stylesheet" href="../css/select.css"/>
    <link type="text/css" rel="stylesheet" href="../css/pageCommon.css"/>
</head>

<body>
<div class="MainContent">

	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">制定计划</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>
	
	<div class="mainindex">
	
		
		<%@ include file="/WEB-INF/include/detailedtask.jsp"%>
		
		<div class="MainNullArea"></div>

		<div class="MainTitle2">我的计划信息 </div>
			
		<%@ include file="/WEB-INF/include/feedbacklist.jsp"%>
		
		
		<div class="buttonArea">
		<input name="" type="button" value="返回" class="InfoButton2" onclick="parent.rightFrame.location.href='PlanServlet?method=findTask'"/>
		<input id="delbtn" type="button" value="删除" class="InfoButton2" /><!-- onclick="return beforeDelete()" -->
		<input id="newplan" type="button"  value="新建计划" name="" class="InfoButton" />
				<!-- onclick="parent.rightFrame.location.href='createnewplan.html'" -->
		<input id="planfeedback" type="button"  value="计划反馈" name="" class="InfoButton" />
				<!-- onclick="parent.rightFrame.location.href='feedbackplan.html'" -->
		</div>
 
	</div> 
	
	<script type="text/javascript">
	
		$("#delbtn").click(function(){
			var taskId =document.getElementById("taskid").value;
			
			var plan=$(":input:checkbox:checked");
			//alert("plan:"+plan);
			var planId="";
			plan.each(function(index,element){
				if($(this).attr("name") != null && $(this).attr("name") != ""){
					planId=planId+","+$(this).attr("name");
				}
			});
			planId=planId.substring(1);
			
			if(planId != null && planId != "" ){
				if(confirm("确认删除？？？")){
					window.location="PlanServlet?method=deletePlan&planId="+planId
					+"&taskId="+taskId;
				}
				
			}else{
				alert("请选择计划 ！");
			} 
			
		});
	
	
			$("#planfeedback").click(function(){
				var plan=$(":input:checkbox:checked");
			//	alert("plan:"+plan);
				if(plan.size() == 1){
					var planId=plan.attr("name"); 
			//		alert(planId);
					if(planId != null && planId != "" ){
						window.location="PlanServlet?method=detailedPlan&planId="+planId;
					}else{
						alert("请选择计划 ！");
					} 
				}else{
					alert("请选择一项计划");
				}
			});
			
			$("#newplan").click(function(){
				
				var taskId =document.getElementById("taskid").value;
			//	alert(taskId)
				if(taskId != null && taskId != "" ){
					window.location="PlanServlet?method=createPlan&taskId="+taskId;
				}else{
					alert("出现错误！");
				} 
				
			});
		
		
		</script>	
 
  <!--<div id="MainAreaEnd"> </div> 
     其他功能超链接 
    <div id="TableTail">
        <div id="TableTail_inside">
            <table border="0" cellspacing="0" cellpadding="4" align="right">
                <tr>
			        <td>
					<A HREF="createnewplan.html"><img src="../img/button/newcreate.jpg" style="margin-right:15px;"/></a>
					<A HREF="myplan.html"><img src="../img/button/delect.jpg"  onClick="return beforeDelete()" /></A>
					
					</td>
                </tr>
			</table>
        </div>
    </div>
-->

</div>
</body>
</html>
