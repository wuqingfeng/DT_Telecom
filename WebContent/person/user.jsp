<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>用户信息</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


</head>

<body>


	<div class="MainContent">

		<div class="MainTop">
			<div class="MainTop_left"></div>

			<div class="MainTitle_begin"></div>
			<div class="MainTitle">人员列表</div>
			<div class="MainTitle_end"></div>

			<div class="MainTop_right"></div>
		</div>

		<div class="mainindex">
			<table cellspacing="0" cellpadding="0" class="tablelist1">
				<!-- 表头-->
				<thead>
					<tr>
						<th width="11%">姓名</th>
						<th width="6%">性别</th>
						<th width="13%">出生时间</th>
						<th width="13%">入职时间</th>
						<th width="">职位描述</th>
						<th width="13%">所属角色</th>
						<th width="10%">上级主管</th>
						<th width="12%">初始密码</th>
						<th width="5%"><input id="checkall" name="" type="checkbox" value=""/></th>
					</tr>
				</thead>



				<!--显示数据列表-->
				<tbody id="TableData">
					<%@ include file="/WEB-INF/include/userlist.jsp"%>
				</tbody>
				
		



			</table>

			<div class="buttonArea">
				<input id="updateButton" name="" type="button" value="详细信息" class="InfoButton"/>
					<!-- onclick="parent.rightFrame.location.href='person/distributionworkerInfo.html'"  -->
				
				<input id="delButton" name="" type="button" value="删除" class="InfoButton2"/> 
				<input name="" type="button"value="添加" class="InfoButton2"
					onclick="parent.rightFrame.location.href='person/adduser.jsp'" />
			</div>

			<script type="text/javascript">
				
				
				
				$("#delButton").click(function(e){
					if($("input:checked").size() == 0){
						alert("请选择要删除的数据");
						return false;
					}
					var userId="";
					$("input:checked").each(function(index,element){
							if(userId != ""){
								userId=userId+","+element.value;
							}else{
								userId=element.value;
							}
					});
					//设置url地址
					var url ="${pageContext.request.contextPath}/ManagerServlet";
							//设置请求参数
				 	var params={"method":"deleteUser1","userIds":userId}; 
					if(confirm("确认要删除数据吗？")){
						$.post(url,params,function(date){
							//若删除成功，则删除选中行
							if(date>0){
								$("input:checked").parents("tr").remove();
							}
						},"text");
					}			
					
					//alert($("input:checked").size());
					//$("#TableData").html("123456789");
					//$("#TableData").load("${pageContext.request.contextPath}/WEB-INF/include/userlist.jsp"); 

					/* //创建一个tr对象
					var $tr=$("<tr></tr>");

					var $tdname=$("<td>"+"username"+"</td>");
					var $tdgender=$("<td>"+"password"+"</td>");
					var $tdbirth=$("<td></td>");
					
					var $tdhiredate=$("<td></td>");
					var $tddescribe=$("<td></td>");
					var $tdrole=$("<td></td>");
					
					var $tdsuperior=$("<td></td>");
					var $tdpassword=$("<td></td>");
					var $tdcheck=$("<td></td>");
					
					var $checkbox=$("<input class='usercheck' name='${supername }' type='checkbox' value='${user.id }' />");
					
					
					$tdcheck.append($checkbox);
					
					$tr.append($tdname);
					$tr.append($tdgender);
					$tr.append($tdbirth);
					$tr.append($tdhiredate);
					$tr.append($tddescribe);
					$tr.append($tdrole);
					$tr.append($tdsuperior);
					$tr.append($tdpassword);
					$tr.append($tdcheck);
					
					

					$("#TableData").append($tr); */
					
				});

				$("#checkall").change(function(){
					if($(this).is(":checked")){
						$(":checkbox").attr("checked","checked");
					}else{
						$(":checkbox").removeAttr("checked");
					}
				});
			
				$("#updateButton").click(function(e){
					
					var userId=$("input:checked")[0].value;
					var supername=$("input:checked")[0].name;
					if($("input:checked").size() == 1 && userId != ""){
						//alert(userId);
						window.location="ManagerServlet?method=findUser&userId="+userId+"&supername="+supername;
						//parent.rightFrame.location.href="person/distributionworkerInfo.jsp"+user;
					}else{
						alert("请选择一条数据！！！");
					}
					
				});
				
				
				/* $("#delButton").click(function(e){
					if($("input:checked").size() == 0){
						alert("请选择要删除的数据");
						return false;
					}
					
					var userId="";
					$("input:checked").each(function(index,element){
							if(userId != ""){
								userId=userId+","+element.value;
							}else{
								userId=element.value;
							}
					});
					//设置url地址
			//		var url ="${pageContext.request.contextPath}/ManagerServlet";
					//设置请求参数
		//			 var params={"method":"deleteUser","userIds":userId}; 
					
					if(confirm("确认要删除数据吗？")){
						 window.location="ManagerServlet?method=deleteUser&userIds="+userId; 
			//			 $.post(url,params,function(data){
			//				 alert(data);
			//				 $("#TableData").html("");
							 
			//			},"json"); 
					}			
				}); */
				
				
			</script>
		</div>

	</div>


</body>
</html>
