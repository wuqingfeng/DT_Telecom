<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>人员信息</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div class="MainContent">

	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">人员列表</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>
	
	<div class="mainindex"> 
		<table cellspacing="0" cellpadding="0" class="tablelist">
		  
			<!-- 表头-->
		   <thead>
				<tr>
					<th width="25%">姓名</th>
					<th width="16%">性别</th>
					<th width="33%">入职时间</th>
					<th width="20%">职位</th>
				    <th width="6%"><input type="radio" name="radiobutton" value="radiobutton" checked="checked" disabled="disabled"/></th>
				</tr>
			</thead>
		
			<!--显示数据列表-->
			<tbody id="TableData" >
			
				<c:forEach items="${page.data }" var="user">
				
				<tr class="TableDetail template">
					<td>${user.username}&nbsp;</td>
					
					<c:choose>
						<c:when test="${user.gender eq '0'}">
							<td >男&nbsp;</td>
						</c:when>
						<c:when test="${user.gender eq '1'}">
							<td >女&nbsp;</td>
						</c:when>
						<c:otherwise>
							<td >&nbsp;</td>
						</c:otherwise>
					</c:choose>
					
					<td>${user.hiredate}&nbsp;</td>
					<td>${user.position}&nbsp;</td>
					<td ><input type="radio" name="radiobutton" value="${user.id }" ></input></td>
				</tr>
				</c:forEach>
				
			</tbody>
			
		</table>
		<div align="center"><%@include file="/WEB-INF/include/page.jsp" %></div>
		
		
		 <div class="buttonArea">
		   <input id="userbutton" name="" type="button"  value="详细信息" class="InfoButton" />
		   <!-- onclick="parent.rightFrame.location.href='workerInfomation.jsp'" -->
	   </div>
	   
	   <script type="text/javascript">
	   		$("#userbutton").click(function(e){
	   			var userId=$(":input:radio:checked").val();
	   			if(userId != "radiobutton"){
	   				window.location="TaskServlet?method=workerInfomation&userId="+userId;
	   			}else{
	   				alert("请选择用户！！");
	   			}
	   		});
	   
	   </script>
	   
   </div>
 
</div>

</body>
</html>
