<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>分配人员</title>
<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>

<body>

<div class="MainContent">
	<div id="Title_bar" class="MainTop">
		<div id="Title_bar_Head" class="MainTop_left"></div>
		<div id="Title_Head"  class="MainTitle_begin"></div>
		<div id="Title" class="MainTitle"><!--页面标题-->分配人员</div>
		<div id="Title_End"  class="MainTitle_end"></div>
		<div id="" class="MainTop_right"></div>
	</div>

	<div id="MainArea" >
		<form action="" style="margin: 0; padding: 0;">
			<center>

				<div id="MainArea_Title">用户信息</div>

				<div id="TableArea" class="mainindex">
					<table cellspacing="0" cellpadding="0" class="TableStyle">

						<!-- 表头-->
						<thead>
							<tr align="center" valign="middle" id="TableTitle">
								<td width="82">姓名</td>
								<td width="48px">性别</td>
								<td width="88px">出生时间</td>
								<td width="88px">入职时间</td>
								<td>职位描述</td>
								<td width="98px">所属角色</td>
								<td>&nbsp;</td>
							</tr>
						</thead>

						<!--显示数据列表-->
						<tbody id="TableData" class="dataContainer"
							datakey="distributionworkerList">
							<tr class="TableDetail template">
								<td>${worker.name}&nbsp;</td>
								<td align="center">${worker.sex}&nbsp;</td>
								<td>${worker.birthtime}&nbsp;</td>
								<td>${worker.beginworktime}&nbsp;</td>
								<td style="padding-left: 0">${worker.postdescribe}&nbsp;</td>
								<td>${worker.role}&nbsp;</td>
								<td align="center"><input name="radiobutton" type="radio"
									value="radiobutton" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- <div id="MainAreaEnd"> </div>
 
  其他功能超链接 -->
				<div id="TableTail" class="buttonArea">
					<div id="TableTail_inside">
						<table border="0" cellspacing="0" cellpadding="4" align="right">
							<tr>
								<td><A HREF="distributionworkerInfo.html"><img
										src="img/button/moreinformation.jpg" /></A></td>
							</tr>
						</table>
					</div>
				</div>


			</center>
		</form>
	</div>

</div>
</body>
</html>
