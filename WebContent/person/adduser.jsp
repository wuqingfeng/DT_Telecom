<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>添加人员</title>
<%@ include file="/WEB-INF/include/base.jsp"%>

	<script type="text/javascript"> 
	$(document).ready(function(e) {
		$(".select1").uedSelect({
			width : 118			  
		});
	});
	</script>

</head>

<body>
<form action="ManagerServlet?method=addUser" method="post">
		<div class="MainContent">

			<div class="MainTop">
				<div class="MainTop_left"></div>

				<div class="MainTitle_begin"></div>
				<div class="MainTitle">添加用户</div>
				<div class="MainTitle_end"></div>

				<div class="MainTop_right"></div>
				
			</div>

			<div class="mainindex">
				<div class="ItemBlock">
					<table border="0" cellspacing="1" cellpadding="1" class="dateTable">

						<tr>
							<th>姓 名</th>
							<td><input type="text" name="username" class="txtInput2" /></td>
							<th>性 别</th>
							<td>
							<select name="gender"  class="select1" >
								<option value="0">男</option>
								<option value="1">女</option>
							</select>
							
							

							
							
							<!-- <input type="text" name="gender" class="txtInput2" /> -->
							</td>
						</tr>
						<tr>
							<th>出生年月</th>
							<td><input name="birthday1" type="date" class="txtInput2"></td>
							<th>入职时间</th>
							<td><input name="hiredate1" type="date" class="txtInput2"></td>
							
						</tr>
						<tr>
							<th>所属角色</th>
							<td>
							
							<select name="role"  class="select1">
								<option value="0">系统管理员</option>
								<option value="1">主管</option>
								<option value="2">普通员工</option>
							</select>
							
							<!-- <input type="text" name="role" class="txtInput2" /> -->
							</td>
							<th>初始密码</th>
							<td><input type="text" name="password" class="txtInput2" /></td>
						</tr>
						<tr>
							<th>职 位</th>
							<td colspan="3"><input type="text" name="position"
								class="txtInput" /></td>
						</tr>
						<tr>
							<th>工作职责</th>
							<td colspan="3"><textarea name="duty" class="TxtArea"></textarea></td>
						</tr>
						<tr>
							<th>行业经历</th>
							<td colspan="3"><textarea name="experience" class="TxtArea"></textarea></td>
						</tr>
					</table>
				</div>

				<div class="buttonArea">
					
					<input name="" type="button" value="重置" class="InfoButton2"
						onclick="this.form.reset();return false;" /> <input name=""
						type="submit" value="提交" class="InfoButton2" />
					<!--  onclick="javascript:history.go(-1);" -->
				</div>

			</div>


		</div>

	</form>

</body>
</html>
