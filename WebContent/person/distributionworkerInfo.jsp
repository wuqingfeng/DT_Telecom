<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>分配人员</title>
<%@ include file="/WEB-INF/include/base.jsp"%>

	<script type="text/javascript"> 
	$(document).ready(function(e) {
		$(".select1").uedSelect({
			width : 118			  
		});
	});
	</script>

</head>

<body>

<div class="MainContent">

	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">人员分配</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>
	<form action="ManagerServlet?method=distrubutionUser" method="post">
	<input type="hidden" name="method" value="distrubutionUser"/>
	<input type="hidden" name="id" value="${user.id }"/>
	<div class="mainindex">

		<div class="ItemBlock">
		
			 <table border="0"cellspacing="0" cellpadding="0" class="dateTable" >       
				<tr>
				  <th>姓　　名</th>
				  <td><input name="username" type="text" class="txtInput2" value="${user.username }"/></td>
				  <th>性　　别</th>
				  <td>
					  <select name="gender" class="select1">
					  	<%-- <c:choose>
							<c:when test="${user.gender eq '0'}">
								<option value="0" disabled select hidden>男</option>
							</c:when>
							<c:when test="${user.gender eq '1'}">
								<option value="1" disabled select hidden>女</option>
							</c:when>
						</c:choose>   --%>
					  	<option value="0">男</option>
					  	<option value="1">女</option>
					  
					  
					  </select>
				</td>
				</tr>
				<tr>
				  <th>学　　历</th>
				  <td><input name="education" type="text" class="txtInput2" value="${user.education }"/></td>
				  <th>出生年月</th>
				  <td ><input name="birthday" type="date" class="txtInput2" value="${user.birthday }"/></td>
				</tr>
				 <tr>
				  <th >专　　业</th>
				  <td><input name="major" type="text" class="txtInput2" value="${user.major }"/></td>
				 <th>入职时间</th>
				 <td><input name="hiredate" type="date" class="txtInput2" value="${user.hiredate }"/></td>
			   </tr>
			   
			   
			   <tr>
				  <th>所属角色</th>
				 
				 <td>
				 <select id="roleselect" name="role" class="select1" title="${user.role } " >
					<%--  <c:choose>
						<c:when test="${user.role eq '0'}">
							<option value="0"  disabled selected hidden>系统管理员</option>
						</c:when>
						<c:when test="${user.role eq '1'}">
							<option value="1" disabled selected hidden>高级主管</option>
						</c:when>
						<c:when test="${user.role eq '2'}">
							<option value="2" disabled selected hidden>普通员工</option>
						</c:when>
					</c:choose>  --%>
					<option value="0">系统管理员</option>
				 	<option value="1">高级主管</option>
				 	<option value="2">普通员工</option>
				</select>
					
				
		
				
				  </td>
				 <th>上级主管</th>
				 <td>
					 <select class="select1" name="superior_id">
						<option disabled selected hidden>${supername }</option>
						 <c:forEach items="${superiorList }" var="superior">
							<option  value="${superior.id }">${superior.username }</option>
						</c:forEach> 
					</select>
				</td>
				</tr>
			   
				<tr>
				  <th>职位描述</th>
				  <td colspan="3"><textarea name="duty" class="TxtArea" >${user.duty } </textarea></td>
			   </tr>	     
			</table>
		</div>
		<div class="buttonArea">
		<input type="submit" value="确定" class="InfoButton2"/> <!-- onclick="javascript:history.go(-1);" -->
		</div>	
  </div> 
  
</form>
</div>


</body>
</html>
