<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>员工详细信息</title>

<%@ include file="/WEB-INF/include/base.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>

<div class="MainContent">

	<div class="MainTop">
		<div class="MainTop_left"></div>
		
		<div class="MainTitle_begin"></div>
		<div class="MainTitle">人员详情</div>
		<div class="MainTitle_end"></div>

		<div class="MainTop_right"></div>
	</div>
	
	<div class="mainindex"> 
	<div class="ItemBlock">
		<table border="0"cellspacing="0" cellpadding="0" class="dateTable" >	
			<tr>
			  <th>姓　　名</th><td><input name="" type="text" class="txtInput2" value="${user.username }" readonly="ture"/></td>
			  <th>性　　别</th>
			  <td>
				  <c:choose>
					<c:when test="${user.gender eq '0'}">
						<input name="" type="text" class="txtInput2" value="男" readonly="ture"/>
					</c:when>
					<c:when test="${user.gender eq '1'}">
						<input name="" type="text" class="txtInput2" value="女" readonly="ture"/>
					</c:when>
					<c:otherwise>
						<input name="" type="text" class="txtInput2" value="" readonly="ture"/>
					</c:otherwise>
				</c:choose>
			  </td>
			</tr>
			<tr>
			  <th>学　　历</th><td><input name="" type="text" class="txtInput2" value="${user.education }" readonly="ture"/></td>
			  <th>出生年月</th><td><input name="" type="text" class="txtInput2" value="${user.birthday }" readonly="ture"/></td>
			</tr>
			<tr>
			  <th>专　　业</th><td><input name="" type="text" class="txtInput2" value="${user.major }" readonly="ture"/></td>
			  <th>入职时间</th><td><input name="" type="text" class="txtInput2" value="${user.hiredate }" readonly="ture"/></td>
			</tr>
			<tr>
			  <th>职　　位</th><td colspan="3"><input name="" type="text" class="txtInput2" value="${user.position }" readonly="ture"/></td>
			</tr>
			<tr>
			  <th>行业经历</th><td colspan="3"><textarea name="" class="TxtArea" readonly="ture">${user.experience }</textarea></td>
			</tr>	     
		</table>
	</div>
	<div class="buttonArea">
		<input type="button" value="返回" class="InfoButton2" onclick="javascript:history.go(-1);"/>
	</div>	
  </div> 
 
</div>


</body>
</html>
