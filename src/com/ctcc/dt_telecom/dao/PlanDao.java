package com.ctcc.dt_telecom.dao;

import java.util.List;

import com.ctcc.dt_telecom.bean.Plan;

/**
 * 定义数据库与计划有关的 基本操作的PlanDao接口
 * @author wp
 *
 */
public interface PlanDao {
	
	/**
	 * 根据taskid获取plan
	 * @param taskId
	 * @return
	 */
	List<Plan> getPlanListByTaskId(String taskId);	
	
	/**
	 * 	根据plan的id获取plan
	 * @param id
	 * @return
	 */
	Plan getPlanById(String id);
	
	/**
	 * 储存plan到数据库
	 * @param plan
	 * @return
	 */
	int savePlan(Plan plan);
	
	/**
	 * 更新数据库中plan
	 * @param plan
	 * @return
	 */
	int updatePlan(Plan plan);
	
	/**
	 * 删除指定的plan的id的 plan
	 * @param id
	 * @return
	 */
	int deletePlan(String id);
	
	/**
	 * 获取所有的计划列表
	 * @return
	 */
	List<Plan>getPlanList();
	
	/**
	 * 根据 计划名称 计划状态 任务名称 中的一个或几个查询计划
	 * @param plan
	 * @return
	 */
	List<Plan>getPlanListByOther(String planname,String planstate,String taskid);


}
