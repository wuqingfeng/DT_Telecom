package com.ctcc.dt_telecom.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.ctcc.dt_telecom.bean.Plan;
import com.ctcc.dt_telecom.utils.JDBCUtils;
import com.sun.org.apache.bcel.internal.generic.NEW;


/**
 * 定义操作数据库的一些方法，专门被其他的DAO 继承
 * @author wp
 *
 * @param <T>
 */
public class BaseDao<T> {
	QueryRunner qr=new QueryRunner();
	Class<T> type;

	public BaseDao() {
		//获得当前类（BaseDao的子类）的 class对象
		Class clazz=this.getClass();
		//返回表示此 clazz 的直接父类的 Type，强转为参数化类型（ParameterizedType类型），如 Collection<String>。 
		ParameterizedType pt = (ParameterizedType)clazz.getGenericSuperclass();
		//System.out.println(pt);
		//返回表示此类型实际类型参数的 Type 对象的数组。
		Type[] types = pt.getActualTypeArguments();
		type= (Class<T>) types[0];
		//System.out.println(type);
	}
	
	/**
	 * 查询一个对象
	 * @param sql
	 * @param params
	 * @return
	 */
	public T getBean(String sql,Object...params) {
		T t=null;
		Connection conn=JDBCUtils.getConnection();
		try {
			t=qr.query(conn, sql, new BeanHandler<>(type),params);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			JDBCUtils.releaseConnection(conn);
		}
		
		return t;
	}
	
	/**
	 * 查询一组对象
	 * @param sql
	 * @param params
	 * @return
	 */
	public List<T>getBeanList(String sql,Object...params){
		List<T> list = null;
		Connection conn=JDBCUtils.getConnection();
		try {
			list =qr.query(conn, sql, new BeanListHandler<>(type), params);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			JDBCUtils.releaseConnection(conn);
		}
		return list;
	}
	
	/**
	 * 储存 更新 或者删除一个对象
	 * @param sql
	 * @param params
	 * @return
	 */
	public int  update(String sql,Object...params) {
		int count=0;
		Connection conn=JDBCUtils.getConnection();
		try {
			count=qr.update(conn, sql, params);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			JDBCUtils.releaseConnection(conn);
		}
		return count;
	}
	public Object  getSingleValue(String sql,Object...params) {
		Object obj=null;
		Connection conn=JDBCUtils.getConnection();
		try {
			obj = qr.query(conn, sql, new ScalarHandler(), params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			JDBCUtils.releaseConnection(conn);
		}
		return obj;
	}
	
	/**
	 * 批量更新 保存 删除
	 * @param sql
	 * @param params
	 * @return
	 */
	public int[] batchUpdate(String sql , Object[][] params) {
		int[] count =null ;
		Connection conn=JDBCUtils.getConnection();
		try {
			count=qr.batch(conn, sql, params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			JDBCUtils.releaseConnection(conn);
		}

		return count;
	}



}
