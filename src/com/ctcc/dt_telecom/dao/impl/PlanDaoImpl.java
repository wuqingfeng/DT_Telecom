package com.ctcc.dt_telecom.dao.impl;

import java.sql.Date;
import java.util.List;

import com.ctcc.dt_telecom.bean.Plan;
import com.ctcc.dt_telecom.dao.BaseDao;
import com.ctcc.dt_telecom.dao.PlanDao;

public class PlanDaoImpl extends BaseDao<Plan> implements PlanDao {

	@Override
	public List<Plan> getPlanListByTaskId(String taskId) {
		String sql="select id, plan_name,plan_describe,plan_begin_time,"
				+ "plan_end_time,plan_state,feedback,task_id  "
				+ " from dt_plan "
				+ " where task_id=?";
		return this.getBeanList(sql, taskId);
	}

	@Override
	public Plan getPlanById(String id) {
		String sql="select id, plan_name,plan_describe,plan_begin_time,"
				+ "plan_end_time,plan_state,feedback,task_id  "
				+ " from dt_plan "
				+ " where id=?";
		return this.getBean(sql, id);
	}

	@Override
	public int savePlan(Plan plan) {
		String sql="insert into dt_plan(plan_name,plan_describe,plan_begin_time" 
				+",plan_end_time ,plan_state ,feedback ,task_id) "
				+ " values(?,?,?,?,?,?,?) ";
		return this.update(sql, plan.getPlan_name(),plan.getPlan_describe(),plan.getPlan_begin_time()
				,plan.getPlan_end_time(),plan.getPlan_state(),plan.getFeedback(),plan.getTask_id());
	} 

	@Override
	public int updatePlan(Plan plan) {
		String sql="update dt_plan set  plan_name =?,plan_describe =?,plan_begin_time =? "
				+ ",plan_end_time =? ,plan_state =? ,feedback =?,task_id =?   "
				+ " where id =? ";
		return this.update(sql, plan.getPlan_name(),plan.getPlan_describe(),plan.getPlan_begin_time()
				,plan.getPlan_end_time(),plan.getPlan_state(),plan.getFeedback(),plan.getTask_id()
				,plan.getId());
	}

	@Override
	public int deletePlan(String id) {
		String sql="delete from dt_plan where id in ("+id+") ";
		//System.out.println(sql);
		return this.update(sql);
	}

	@Override
	public List<Plan> getPlanList() {
		
		return null;
	}

	@Override
	public List<Plan> getPlanListByOther(String planname,String planstate,String taskid) {
		String sql="select * from dt_plan "
				+ " where plan_name like ?  and plan_state like ? and task_id like ? ";
		
		
		return this.getBeanList(sql,planname,planstate,taskid);
	}

	
}
