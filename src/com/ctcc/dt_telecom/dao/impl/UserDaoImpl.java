package com.ctcc.dt_telecom.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import com.ctcc.dt_telecom.bean.Page;
import com.ctcc.dt_telecom.bean.User;
import com.ctcc.dt_telecom.dao.BaseDao;
import com.ctcc.dt_telecom.dao.UserDao;

public class UserDaoImpl extends BaseDao<User> implements UserDao {

	@Override
	public User getUserByUserNameAndPassword(User user) {
		String sql="select id,username,gender,education,major,birthday,"
				+ "hiredate,role,superior_id,password,position,duty,experience " 
				+ " from dt_user "
				+ " where username =? and password =? and role =?";
		return this.getBean(sql, user.getUsername(),user.getPassword(),user.getRole() );
	}

	@Override
	public List<User> getUserList() {
		String sql="select id,username,gender,education,major,birthday," 
				+"hiredate,role,superior_id,password,position,duty,experience "  
				+" from dt_user";
		return this.getBeanList(sql);
	}



	@Override
	public User getUserById(String id) {
		String sql="select id,username,gender,education,major,birthday," 
				+"hiredate,role,superior_id,password,position,duty,experience "  
				+" from dt_user "
				+ "where id=?";
		return this.getBean(sql, id);
	}

	@Override
	public int saveUser(User user) {
		String sql="insert into dt_user(username,gender,education,major,birthday," + 
				"hiredate,role,superior_id,password,position,duty,experience)   "  
				+" values(?,?,?,?,?,?,?,?,?,?,?,?)";
		return this.update(sql, user.getUsername(),user.getGender(),user.getEducation(),user.getMajor(),user.getBirthday()
				,user.getHiredate(),user.getRole(),user.getSuperior_id(),user.getPassword(),user.getPosition()
				,user.getDuty(),user.getExperience());
	}

	@Override
	public int updateUser(User user) {
		String sql="update dt_user set username =? ,gender =? ,education =? ,major =? ,birthday =? ," + 
				"hiredate =?,role =? ,superior_id =? ,duty =?   "  
				+" where id=?";
		return this.update(sql, user.getUsername(),user.getGender(),user.getEducation(),user.getMajor(),user.getBirthday()
				,user.getHiredate(),user.getRole(),user.getSuperior_id()
				,user.getDuty(),user.getId());
	}

	@Override
	public int deleteUser(String userIds) {
		String sql="delete from dt_user where id in ("+userIds+") ";
	//	System.out.println(sql);
		return this.update(sql);
	}

	@Override
	public List<User> getUserByRole(String role) {
		String sql="select id,username,gender,education,major,birthday,"
				+ "hiredate,role,superior_id,password,position,duty,experience " 
				+ " from dt_user "
				+ " where role =?";
		return this.getBeanList(sql, role);
	
	}
	
	@Override
	public List<User> getUserBySuperiorId(Integer superiorId) {
		String sql="select id,username,gender,education,major,birthday,"  
				+" hiredate,role,superior_id,password,position,duty,experience "
				+ " from dt_user "
				+ " where superior_id=?";
		return this.getBeanList(sql, superiorId);
	}

	@Override
	public Page<User> getUserBySuperiorId(Page<User> page,User user) {
		//获取该主管 下属的总数量
		String sql="select count(*) from dt_user where superior_id=?";

		Integer superiorId=user.getId() ;

		BigDecimal singleValue = (BigDecimal)this.getSingleValue(sql, superiorId );

		int totalRecord=singleValue.intValue();
		
	//	System.out.println(singleValue.getClass());
	//	System.out.println(totalRecord);
		page.setTotalRecord(totalRecord);
		
		sql="select id,username,gender,education,major,birthday," 
				+ " hiredate,role,superior_id,password,position,duty,experience "  
				+ " from "
				+ " (select t.*,rownum as rn from dt_user t where superior_id =? )  " 
				+ " where rn>? and rn <=?  ";
		
		List<User> data = this.getBeanList(sql, superiorId,page.getIndex(),page.getIndex()+page.getPageSize());
		//System.out.println(data);
		//将数据保存在page中
		page.setData(data);
		return page;
	}

}
