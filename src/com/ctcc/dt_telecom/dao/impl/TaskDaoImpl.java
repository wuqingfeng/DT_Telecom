package com.ctcc.dt_telecom.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import com.ctcc.dt_telecom.bean.Page;
import com.ctcc.dt_telecom.bean.Task;
import com.ctcc.dt_telecom.bean.User;
import com.ctcc.dt_telecom.dao.BaseDao;
import com.ctcc.dt_telecom.dao.TaskDao;

public class TaskDaoImpl extends BaseDao<Task> implements TaskDao {

	@Override
	public int saveTask(Task task) {
		String sql="insert into dt_task(task_name,task_describe, "
				+ "task_begin_time,task_end_time,task_state, user_id) "
				+ " values(?,?,?,?,?,?)";
		return this.update(sql, task.getTask_name(),task.getTask_describe(),
				task.getTask_begin_time(),task.getTask_end_time(),
				task.getTask_state(),task.getUser_id());
	}

	@Override
	public Page<Task> getTaskListByUserIds(Page<Task> page, String userIds, String task_state) {
		//查询 该用户 所有下属的 所有任务的 数量
		String sql="select count(*) from dt_task "
				+ "where user_id in("+ userIds +") and task_state in("+task_state+")";
		Object singleValue = this.getSingleValue(sql);
	//	System.out.println(singleValue.getClass());//class java.math.BigDecimal
		
		int totalRecord=((BigDecimal)singleValue).intValue();
		page.setTotalRecord(totalRecord);
		/*sql="select id,task_name,task_describe,task_begin_time,"
				+ "task_end_time,task_state, user_id "
				+ " from dt_task "
				+ " where user_id in("+userIds+") ";*/
		
		sql="select id,task_name,task_describe,task_begin_time,"
				+ "task_end_time,task_state, user_id "
				+ " from (select t.*,rownum as rn from dt_task t "
				+ "where user_id in("+userIds+") and task_state in("+task_state+") ) " 
				+ " where rn>? and rn <=? ";
		
		
	//	System.out.println(sql);
		List<Task> data = this.getBeanList(sql,page.getIndex(),page.getIndex()+page.getPageSize());
		
		page.setData(data);
		
		return page;
	}

	@Override
	public Task getTaskById(String taskId) {
		String sql="select id,task_name,task_describe,task_begin_time,"
				 + "task_end_time,task_state, user_id  "
				 + " from  dt_task "
				 + " where id=?";
		//System.out.println(sql);
		return this.getBean(sql, taskId);
	}

	@Override
	public Task getTaskByState(String state) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int updateTask(Task task) {
		String sql="update dt_task set task_name=?,task_describe=?,task_begin_time=?,"
				 + "task_end_time=?,task_state=?, user_id=?  "
				 + " where id=?";
		
		return this.update(sql, task.getTask_name(),task.getTask_describe(),
				task.getTask_begin_time(),task.getTask_end_time(),
				task.getTask_state(),task.getUser_id(),task.getId());
		
	}

	@Override
	public int deleteTask(String id) {
		String sql="delete from dt_task where id=? ";
		return this.update(sql, id);
	}

	@Override
	public List<Task> getTaskListByUser(User user) {
		String sql="select id,task_name,task_describe,task_begin_time,"
				+ " task_end_time,task_state, user_id "
				+ " from dt_task where user_id=?";
		return this.getBeanList(sql, user.getId());
	}



	

}
