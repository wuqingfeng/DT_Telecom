package com.ctcc.dt_telecom.dao;

import java.util.List;

import com.ctcc.dt_telecom.bean.Page;
import com.ctcc.dt_telecom.bean.Task;
import com.ctcc.dt_telecom.bean.User;

/**
 * 定义任务task与数据库交互的方法
 * @author wp
 *
 */
public interface TaskDao {
	
	/**
	 * 储存task到数据库
	 * @param task
	 * @return
	 */
	int saveTask(Task task);
	/**
	 * 根据任务状态 从数据库中查找 登录用户 全部下属的 全部的task 
	 * @param page
	 * @param userIds
	 * @param task_state
	 * @return
	 */
	Page<Task> getTaskListByUserIds(Page<Task> page,String userIds , String task_state);
	
	/**
	 * 根据task的id获取task
	 * @param taskId
	 * @return
	 */
	Task getTaskById(String taskId);
	
	/**
	 * 根据任务的状态state  获取task，  任务状态 0未实施,1实施中,2已完成
	 * @param state
	 * @return
	 */
	Task getTaskByState(String state);
	
	/**
	 * 更新数据库中的task
	 * @param task
	 * @return
	 */
	int updateTask(Task task);	
	
	/**
	 * 根据id删除数据库中的task
	 * @param taskId
	 * @return
	 */
	int deleteTask(String id);
	
	/**
	 * 根据上级获取任务，即根据user获取task
	 * @param user
	 * @return
	 */
	List<Task>getTaskListByUser(User user);


}
