package com.ctcc.dt_telecom.dao;

import java.util.List;

import com.ctcc.dt_telecom.bean.Page;
import com.ctcc.dt_telecom.bean.User;



/**
 * 定义数据库的基本操作的UserDao接口
 * @author wp
 *
 */
public interface UserDao {
	
	/**
	 * 通过用户名密码和用户角色获取用户  用于用户登录
	 * @param user
	 * @return
	 */
	User getUserByUserNameAndPassword(User user);
	
	/**
	 * 获取所有的用户
	 * @return
	 */
	List<User>getUserList();	
	
	/**
	 * 根据上级的id 查询员工
	 * @param superiorId
	 * @return
	 */
	List<User> getUserBySuperiorId(Integer superiorId);	
	
	/**
	 * 分页 根据上级的id 查询员工
	 * @param superiorId
	 * @return
	 */
	Page<User> getUserBySuperiorId(Page<User> page,User user);
	
	
	/**
	 * 根据id获取User对象
	 * @param id
	 * @return
	 */
	User getUserById(String id);
	
	/**
	 * 获取相应角色的用户  role
	 * @return
	 */
	List<User> getUserByRole(String role);
	
	/**
	 * 储存User对象到数据库
	 * @param user
	 * @return
	 */
	int saveUser(User user);
	/**
	 * 更新数据库中的User 对象
	 * @param user
	 * @return
	 */
	int updateUser(User user);
	
	/**
	 * 
	 * 删除根据一组userId属性 删除一组数据
	 * @param userId
	 * @return
	 */
	int deleteUser(String userIds);	
	
	 
	

}
