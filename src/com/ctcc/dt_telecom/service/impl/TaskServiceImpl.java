package com.ctcc.dt_telecom.service.impl;

import java.util.List;

import com.ctcc.dt_telecom.bean.Page;
import com.ctcc.dt_telecom.bean.Task;
import com.ctcc.dt_telecom.bean.User;
import com.ctcc.dt_telecom.dao.TaskDao;
import com.ctcc.dt_telecom.dao.UserDao;
import com.ctcc.dt_telecom.dao.impl.TaskDaoImpl;
import com.ctcc.dt_telecom.dao.impl.UserDaoImpl;
import com.ctcc.dt_telecom.service.TaskService;


public class TaskServiceImpl implements TaskService {
	TaskDao taskDao=new TaskDaoImpl();
	UserDao userDao=new UserDaoImpl();
	@Override
	public int saveTask(Task task) {
		
		return taskDao.saveTask(task);
	}
	@Override
	public Page<Task> getTaskListByUser(List<User> userList, String pageNumber, int pageSize,  String task_state) {
		
		String userIds="";
		for (User user : userList) {
			userIds=userIds+","+user.getId();
		}
		userIds=userIds.substring(1);

		int pn=1;
		try {
			pn=Integer.parseInt(pageNumber);
		} catch (Exception e) {}
		Page<Task> page=new Page<>();
		page.setPageNumber(pn);
		page.setPageSize(pageSize);
		
		return taskDao.getTaskListByUserIds(page, userIds, task_state);
	}
	@Override
	public Task getTaskById(String taskId) {
		return taskDao.getTaskById(taskId);
	}
	@Override
	public Task getTaskByState(String state) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int updateTask(Task task) {
		
		return taskDao.updateTask(task);
	}
	@Override
	public int deleteTask(String id) {
		
		return taskDao.deleteTask(id);
	}
	@Override
	public List<Task> getTaskListBySuperiorId(String superior_id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<Task> getTaskListByUser(User user) {
		
		return taskDao.getTaskListByUser(user);
	}
	
	

}
