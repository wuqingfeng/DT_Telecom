package com.ctcc.dt_telecom.service.impl;

import java.util.List;

import com.ctcc.dt_telecom.bean.Page;
import com.ctcc.dt_telecom.bean.User;
import com.ctcc.dt_telecom.dao.UserDao;
import com.ctcc.dt_telecom.dao.impl.UserDaoImpl;
import com.ctcc.dt_telecom.service.UserService;

public class UserServiceImpl implements UserService {
	UserDao userDao=new UserDaoImpl();
	@Override
	public User login(User user) {
		return userDao.getUserByUserNameAndPassword(user);
	}
	
	@Override
	public List<User> getUserList() {
		
		return userDao.getUserList();
	}

	@Override
	public int addUser(User user) {
		return userDao.saveUser(user);
	}

	@Override
	public int distributionUser(User user) {
		
		return userDao.updateUser(user);
	}

	@Override
	public int deleteUser(String userIds) {
		
		return userDao.deleteUser(userIds);
	}

	@Override
	public User getUserByUserId(String userId) {
		
		return userDao.getUserById(userId);
	}

	@Override
	public List<User> getSuperiorByRole() {
		
		return userDao.getUserByRole("1");
	}

	@Override
	public List<User> getUserBySuperiorId(User loginUser) {
		
		return userDao.getUserBySuperiorId(loginUser.getId());
	}

	@Override
	public Page<User> getUserBySuperiorId(User user, String pageNumber, int pageSize) {
		int pn=1;
		try {
			//若转换失败 默认为1
			pn=Integer.parseInt(pageNumber);
		} catch (Exception e) {
			
		}
		Page<User> page=new Page<>();
		page.setPageNumber(pn);
		page.setPageSize(pageSize);
		return userDao.getUserBySuperiorId(page,user);
	}

}
