package com.ctcc.dt_telecom.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ctcc.dt_telecom.bean.Plan;
import com.ctcc.dt_telecom.dao.PlanDao;
import com.ctcc.dt_telecom.dao.impl.PlanDaoImpl;
import com.ctcc.dt_telecom.service.PlanService;

public class PlanServiceImpl implements PlanService {
	private PlanDao planDao=new PlanDaoImpl();

	@Override
	public int savePlan(Plan plan) {
		
		return planDao.savePlan(plan);
	}

	@Override
	public int updatePlan(Plan plan) {
		
		return planDao.updatePlan(plan);
	}

	@Override
	public int deletePlan(String id) {
		return planDao.deletePlan(id);
	}

	@Override
	public List<Plan> getPlanList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Plan> getPlanListByTaskId(String taskId) {
		return planDao.getPlanListByTaskId(taskId);
	}

	@Override
	public Plan getPlanById(String id) {
		
		return planDao.getPlanById(id);
	}

	@Override
	public List<Plan> getPlanListByOther(Plan plan, Map<String, Long> date) {

		if(plan.getPlan_name() =="") {
			plan.setPlan_name("%");
		}
		if(plan.getPlan_state() == "") {
			plan.setPlan_state("%");
		}
		String taskid = "%";
		if(plan.getTask_id() != -1 ) {
			taskid=plan.getTask_id().toString();
		}
		
		List<Plan> list = planDao.getPlanListByOther(plan.getPlan_name(), plan.getPlan_state(), taskid);

		List<Plan> listPlan=new ArrayList<>();
		for (Plan p : list) {
			Date begin_time=p.getPlan_begin_time();
			if(begin_time != null ) {
				long begin = begin_time.getTime();
				if (date.get("1b") > begin || date.get("1e")< begin) {
					continue;
				}
			}else if(date.get("1b") !=0 || date.get("1e") != Long.MAX_VALUE  ) {
				continue;
			}
			
			Date end_time=p.getPlan_end_time();
			if(end_time !=null) {
				long end = end_time.getTime();
				if (date.get("2b") > end || date.get("2e")< end) {
					continue;
				}
			}else if(date.get("2b") !=0 || date.get("2e") != Long.MAX_VALUE  ) {
				continue;
			}
			
			listPlan.add(p);
			
		}
		
		return listPlan;
	}

}
