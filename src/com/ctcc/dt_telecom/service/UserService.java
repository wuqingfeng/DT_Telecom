package com.ctcc.dt_telecom.service;

import java.util.List;

import com.ctcc.dt_telecom.bean.Page;
import com.ctcc.dt_telecom.bean.User;

/**
 * 处理与User相关的业务
 * @author wp
 *
 */
public interface UserService {
	
	/**
	 * 用户登录  返回null 代表没有此对象
	 * @param user
	 * @return
	 */
	User login(User user);
	
	/**
	 * 获取所有的用户
	 * @return
	 */
	List<User>getUserList();	
	
	
	/**
	 * 管理员向数据库中添加人员
	 * @param user
	 * @return
	 */
	int addUser(User user);
	
	/**
	 * 分配人员  向数据库更新数据
	 * @param user
	 * @return
	 */
	int distributionUser(User user);
	
	/**
	 * 删除人员 
	 * @return
	 */
	int deleteUser(String userIds);
	
	/**
	 * 根据用户的id属性从数据库中获取用户
	 * @param usersId
	 * @return
	 */
	User getUserByUserId(String userId);
	
	/**
	 * 获取所有员工的上级的数据，即所有角色 role=1 的人
	 * @param role
	 * @return
	 */
	List<User> getSuperiorByRole();
	
	/**
	 * 根據 登錄系統的主管的id號碼 查詢主管的下屬
	 * @param user
	 * @return
	 */
	List<User> getUserBySuperiorId(User loginUser);
	
	/**
	 * 根据已登录的用户的 获取其下属
	 * @param user
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	Page<User> getUserBySuperiorId(User user,String pageNumber,int pageSize );

}
