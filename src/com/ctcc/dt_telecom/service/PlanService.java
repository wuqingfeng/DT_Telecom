package com.ctcc.dt_telecom.service;

import java.util.List;
import java.util.Map;

import com.ctcc.dt_telecom.bean.Plan;

/**
 * 定义与plan有关的服务
 * @author wp
 *
 */
public interface PlanService {
	
	/**
	 * 根据taskid获取plan
	 * @param taskId
	 * @return
	 */
	List<Plan> getPlanListByTaskId(String taskId);	
	
	/**
	 * 	根据plan的id获取plan
	 * @param id
	 * @return
	 */
	Plan getPlanById(String id);
	
	/**
	 * 储存plan到数据库
	 * @param plan
	 * @return
	 */
	int savePlan(Plan plan);
	
	/**
	 * 更新数据库中plan
	 * @param plan
	 * @return
	 */
	int updatePlan(Plan plan);
	
	/**
	 * 删除指定的plan的id的 plan
	 * @param id
	 * @return
	 */
	int deletePlan(String id);
	
	/**
	 * 获取所有的计划列表
	 * @return
	 */
	List<Plan>getPlanList();
	/**
	 * 根据一个计划中的已知项 查询未知项 
	 * @param plan
	 * @param eendtime 
	 * @param ebegintime 
	 * @param bendtime 
	 * @param bbegintime 
	 * @return
	 */
	List<Plan>getPlanListByOther(Plan plan,Map<String, Long> date);

}
