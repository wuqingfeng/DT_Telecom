package com.ctcc.dt_telecom.service;

import java.util.List;

import com.ctcc.dt_telecom.bean.Page;
import com.ctcc.dt_telecom.bean.Task;
import com.ctcc.dt_telecom.bean.User;

public interface TaskService {
	/**
	 * 向数据库中插入一条任务
	 * @param task
	 * @return
	 */
	int saveTask(Task task);

	/**
	 * 根据已经登录的用户 和任务状态   获取该用户所有下属的 所有任务task
	 * @param userList
	 * @param pageNumber
	 * @param pageSize
	 * @param task_state
	 * @return
	 */
	Page<Task> getTaskListByUser(List<User> userList, String pageNumber, int pageSize,  String task_state);
	
	/**
	 * 根据执行任务的人 查询任务
	 * @param user
	 * @return
	 */
	List<Task> getTaskListByUser(User user);

	/**
	 * 根据task的id获取task
	 * @param taskId
	 * @return
	 */
	Task getTaskById(String taskId);
	
	/**
	 * 根据任务的状态state  获取task，  任务状态 0未实施,1实施中,2已完成
	 * @param state
	 * @return
	 */
	Task getTaskByState(String state);
	
	/**
	 * 更新数据库中的task
	 * @param task
	 * @return
	 */
	int updateTask(Task task);	
	
	/**
	 * 根据id删除数据库中的task
	 * @param taskId
	 * @return
	 */
	int deleteTask(String id);
	
	/**
	 * 根据上级获取任务，先根据superId获取userId,在获取task
	 * @param superior_id
	 * @return
	 */
	List<Task>getTaskListBySuperiorId(String superior_id);


}
