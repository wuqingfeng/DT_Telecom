package com.ctcc.dt_telecom.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.ctcc.dt_telecom.bean.User;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;



class TestBeanUtils {

	@Test
	void test() {
		User user=new User();
		Map<String, String[]> map = new HashMap<String, String[]>();
		map.put("username",new String[] {"admin"} );
		map.put("password",new String[] {"123"} );
		map.put("duty",new String[] {"随便"} );
		map.put("birthday", new String[] {"2018-06-24"});
		
		String strDate="2018-06-28";
        //注意：SimpleDateFormat构造函数的样式与strDate的样式必须相符
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
      //  SimpleDateFormat sDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //加上时间
        //必须捕获异常
        try {
        	Date date = simpleDateFormat.parse(strDate);
        	System.out.println(date);
        	
        	String string = simpleDateFormat.format(date);
        	System.out.println(string);
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
			
	}

}
