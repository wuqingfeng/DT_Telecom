package com.ctcc.dt_telecom.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.junit.jupiter.api.Test;

import com.ctcc.dt_telecom.utils.JDBCUtils;

class TestJDBCUtils {

	@Test
	void test() {
		Connection conn = JDBCUtils.getConnection();
		System.out.println(conn);
		String sql="select * from dt_user";
		
		QueryRunner qr=new QueryRunner();
		try {
			List<Map<String, Object>> list = qr.query(conn, sql, new MapListHandler());
			for (Map<String, Object> map : list) {
				System.out.println(map.entrySet());
			}
		
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JDBCUtils.releaseConnection(conn);
		System.out.println(conn);
	}

}
