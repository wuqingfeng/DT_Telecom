package com.ctcc.dt_telecom.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.ctcc.dt_telecom.bean.Plan;
import com.ctcc.dt_telecom.dao.PlanDao;
import com.ctcc.dt_telecom.dao.impl.PlanDaoImpl;

class TestPlanDao {
	private PlanDao planDao=new PlanDaoImpl();

	@Test
	void testGetPlanListByOther() {
		Plan plan=new Plan();
	//	plan.setPlan_state("%");
		plan.setPlan_name("计划一");
		
		String planname = "%";
		String planstate ="%";
		String taskid = "%";
		List<Plan> list = planDao.getPlanListByOther(planname, planstate, taskid);
		System.out.println(list);
		System.out.println("----------------");
		for (Plan p : list) {
			System.out.println(p);
		}
	}
	
	@Test
	void textif() {
		for (int i = 0; i <10; i++) {
			if(i % 2 ==0) {
				continue;
			}
			System.out.println(i);
			
		}
	}
	@Test
	void testLong() {
		System.out.println(Long.MAX_VALUE);
		System.out.println(Long.MIN_VALUE);
		System.out.println(Long.MIN_VALUE <8);
	}

}
