package com.ctcc.dt_telecom.test;


import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.ctcc.dt_telecom.bean.Task;
import com.ctcc.dt_telecom.bean.User;
import com.ctcc.dt_telecom.service.UserService;
import com.ctcc.dt_telecom.service.impl.UserServiceImpl;

class TestUserService {
	UserService userService=new UserServiceImpl();
	@Test
	void testLogin() {
		//User user=new User(null, "admin", '男', "本科", "物理", null, null, '0', 0, "admin", null, null, null);
		User user=new User();
		user.setUsername("admin");
		user.setPassword("admin");
		user.setRole("0");
		User loginUser = userService.login(user);
		System.out.println(loginUser);
	}
	
	@Test
	void testGetUserList() {
		
		  List<User> userList = userService.getUserList();
		for (User user : userList) {
			System.out.println(user);
		}
	}
	@Test
	void testAddUser() {
		//User user1=new User(id, username, gender, education, major, birthday, hiredate, role, superior_id, password, position, duty, experience)
		//User user=new User(null, "UUU", "0", "FSDA","FDSA", new Date(),new Date(), "0", 55, "111", null, "fsa","ff");
		//User user=new User(null, "UUU", "0", "FSDA","FDSA", new Date(),new Date(), "0", 55, "111", null, "fsa","ff");
		User user=new User();
		user.setUsername("fsadf");
		user.setRole("0");
		Date date=new Date(new java.util.Date().getTime());
		System.out.println(date);
		user.setBirthday(date);
		int count=userService.addUser(user);
		System.out.println(count);
	}
	
	@Test
	void testDistributionUser() {
		//User user=new User(0, "admin", null, null,"FDSA", new Date(2015),new Date(555), "0", 32, "admin", null, "fsa","ff");
		User user=new User();
		user.setId(0);
		user.setUsername("admin");
		user.setPassword("admin");
		int count =userService.distributionUser(user);
		System.out.println(count);
	}
	
	@Test
	void testDeleteUser() {
		String userIds="64,66,67";
		int count=userService.deleteUser(userIds);
		System.out.println(count);
	}
	@Test
	void testList() {
		List<Task> list=new ArrayList<>();
		list.add(new Task(1, "ww", "ee", null, null,null, 2));
		list.add(new Task(2, "ww", "ee", null, null,null, 2));
		list.add(new Task(3, "ww", "ee", null, null,null, 2));
		list.add(new Task(4, "ww", "ee", null, null,null, 2));
		String userIds="";
		
		for (Task user : list) {
			userIds=userIds+","+user.getId();
		}
		System.out.println(userIds);
		userIds=userIds.substring(1);
		System.out.println(userIds);
	}

	

}
