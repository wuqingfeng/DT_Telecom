package com.ctcc.dt_telecom.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.ctcc.dt_telecom.bean.Plan;
import com.ctcc.dt_telecom.bean.User;
import com.ctcc.dt_telecom.dao.BaseDao;

class TestBaseDao extends BaseDao<Plan> {

	@Test
	void testBaseDao() {
		
	}

	@Test
	void testGetBean() {
		String sql="select * from dt_plan";
		 Plan plan = this.getBean(sql);
		System.out.println(plan);
		
	}

	@Test
	void testGetBeanList() {
		String sql="select * from dt_plan ";
		 List<Plan> list = this.getBeanList(sql);
		System.out.println(list);
		
	}

	@Test
	void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	void testGetSingleValue() {
		fail("Not yet implemented");
	}

	@Test
	void testBatchUpdate() {
		fail("Not yet implemented");
	}

}
