package com.ctcc.dt_telecom.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ctcc.dt_telecom.bean.User;


public class ClerkLoginFilter extends HttpFilter implements Filter {

    
	public void destroy() {
		
	}

	@Override
	public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpSession session=request.getSession();
		//获取当前登录用户
		User loginUser = (User) session.getAttribute("loginUser");
		//System.out.println(loginUser);
		if(loginUser == null) {
			
			// 设置一个错误消息
			request.setAttribute("msg", "该操作需要用户登录");
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		}else if("1".equals(loginUser.getRole())){
			// 放行
			chain.doFilter(request, response);
		}else {
			// 设置一个错误消息
			request.setAttribute("msg", "该操作需要主管登录");
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		}
		
	}

	
}
