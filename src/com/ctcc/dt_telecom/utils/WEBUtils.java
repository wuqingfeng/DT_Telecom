package com.ctcc.dt_telecom.utils;

import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;

import com.ctcc.dt_telecom.bean.Plan;
import com.ctcc.dt_telecom.bean.Task;
import com.ctcc.dt_telecom.bean.User;

public class WEBUtils {
	
	

	
	/**
	 * 将map类型转换为bean
	 * @param request
	 * @param t
	 * @return
	 */
	public static <T> T paramToBean(HttpServletRequest request,T t) {
		//通过request从服务器获取参数的map集合
		Map<String, String[]> map = request.getParameterMap();
		try {
			//使用BeanUtils.populate 将map转为bean
			BeanUtils.populate(t, map);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return t;
	}
	
	public static Task paramToBean(HttpServletRequest request,Task task) {
		Map<String, String[]> map = request.getParameterMap();
		String task_begin_time1=request.getParameter("task_begin_time1");
		String task_end_time1=request.getParameter("task_end_time1");
	//	System.out.println(task_begin_time1);
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			BeanUtils.populate(task, map);
			if(task_begin_time1 != null && task_begin_time1 !="") {
				Date task_begin_time=new Date(simpleDateFormat.parse(task_begin_time1).getTime());
				BeanUtils.setProperty(task, "task_begin_time", task_begin_time);
			}
			
			if(task_end_time1 != null && task_end_time1 !="") {
				Date task_end_time=new Date(simpleDateFormat.parse(task_end_time1).getTime());
				BeanUtils.setProperty(task, "task_end_time", task_end_time);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return task;
	}
	
	
	public static Plan paramToBean(HttpServletRequest request,Plan plan) {
		Map<String, String[]> map = request.getParameterMap();
		String plan_begin_time1=request.getParameter("plan_begin_time1");
		String plan_end_time1=request.getParameter("plan_end_time1");
		
	//	System.out.println(plan_begin_time1);
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			BeanUtils.populate(plan, map);
			if(plan_begin_time1 != null && plan_begin_time1 !="") {
				Date plan_begin_time=new Date(simpleDateFormat.parse(plan_begin_time1).getTime());
				
				BeanUtils.setProperty(plan, "plan_begin_time", plan_begin_time);
			}
			
			if(plan_end_time1 != null && plan_end_time1 !="") {
				Date plan_end_time=new Date(simpleDateFormat.parse(plan_end_time1).getTime());
				BeanUtils.setProperty(plan, "plan_end_time", plan_end_time);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return plan;
	}
	
	/**
	 * 将map转为user
	 * 思路：
	 * 1.将前端input 中有关日期的name都改为别名，即在原名前面加1，
	 *   所以该值在使用BeanUtils.populate(user, map) 转换时就会为null，不会出现问题
	 * 2. 再用request.getParameter获取未转换的日期类型，用SimpleDateFormat手动将string转为java.util.sql
	 * 3.使用 new java.sql.Date(long)的方法，将java.util.Date转为ava.sql.Date方法
	 * 4.再使用BeanUtils.setProperty将其存进user中
	 * 
	 * @param request
	 * @param user
	 * @return
	 */
	public static User paramToBean(HttpServletRequest request,User user) {
		Map<String, String[]> map = request.getParameterMap();
		String birthday1=request.getParameter("birthday1");
		String hiredate1=request.getParameter("hiredate1");
		
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
		try {
			BeanUtils.populate(user, map);
			if(birthday1 != null && birthday1 !="") {
				Date birthday =new Date(simpleDateFormat.parse(birthday1).getTime());
				BeanUtils.setProperty(user, "birthday", birthday);
			}
			if(hiredate1  !=null && hiredate1 !="") {
				Date hiredate =new Date(simpleDateFormat.parse(hiredate1).getTime());
				BeanUtils.setProperty(user, "hiredate", hiredate);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} 
		return user;
	}

	
	public static String getPath(HttpServletRequest request,HttpServletResponse response) {
		//动态获取请求的求地址
		String uri=request.getRequestURI();
		//System.out.println(uri);///BookStore_DF5/manager/BookManagerServlet
		//获取查询字符串
		String queryString=request.getQueryString();
	//	System.out.println(queryString);//method=findBook
		String path=uri +"?" + queryString;
	//	System.out.println(path);
		//判断path中是否包含 &pagenumber字符串
		if(path.contains("&pageNumber")) {
			path=path.substring(0, path.indexOf("&pageNumber"));
		}
		return path;
	}
	
	/**
	 * 获取已登录用户的信息
	 * @param request
	 * @return
	 */
	public static User getLoginUser(HttpServletRequest request) {
		HttpSession session=request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		return loginUser;
	} 
	
	/**
	 * 用于数据查询时获取查询的日期范围
	 * @param request
	 * @return
	 */
	public static Map<String, Long> getQueryDate(HttpServletRequest request){
		String bbegintime=request.getParameter("bbegintime");
		String bendtime=request.getParameter("bendtime");
		String ebegintime=request.getParameter("ebegintime");
		String eendtime=request.getParameter("eendtime");
		Map<String, Long> date=new HashMap<>();
	//	System.out.println(bbegintime);
		
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
		try {
			if(bbegintime != null && bbegintime !="") {
				long bbtime = simpleDateFormat.parse(bbegintime).getTime();
				date.put("1b", bbtime);
			}else {
				date.put("1b", 0L);
			}
			if(bendtime !=null && bendtime != "") {
				long betime = simpleDateFormat.parse(bendtime).getTime();
				date.put("1e", betime);
			}else {
				date.put("1e", Long.MAX_VALUE);
			}
			if(ebegintime !=null && ebegintime != "") {
				long ebtime = simpleDateFormat.parse(ebegintime).getTime();
				date.put("2b", ebtime);
			}else {
				date.put("2b", 0L);
			}
			if(eendtime !=null && eendtime != "") {
				long eetime = simpleDateFormat.parse(eendtime).getTime();
				date.put("2e", eetime);
			}else {
				date.put("2e", Long.MAX_VALUE);
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		return date;
		
	}
}
