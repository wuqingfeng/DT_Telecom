package com.ctcc.dt_telecom.utils;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class JDBCUtils {
	private static DataSource ds=new ComboPooledDataSource("helloc3p0");
	
	//private static Map<K, V>
	
	/**
	 * 连接数据库
	 * @return
	 */
	public static Connection getConnection() {
		Connection conn=null;
		try {
			conn=ds.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return conn;
	}
	
	/**
	 * 关闭数据库
	 * @param conn
	 */
	public static void releaseConnection(Connection conn) {
		if(conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	

}
