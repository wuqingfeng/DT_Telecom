package com.ctcc.dt_telecom.bean;

import java.sql.Date;

/**
 * 用于封装与任务相关的信息
 * @author wp
 *
 */
public class Task {
	
	private Integer id; //主键自增
	private String task_name;//	任务名称
	private String task_describe;//	任务描述
	private Date task_begin_time;//任务开始时间
	private Date task_end_time;//任务结束时间
	private String task_state;	//	任务状态 0未实施,1实施中,2已完成
	private Integer user_id;//	实施人的id  user表的id
	public Task() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Task(Integer id, String task_name, String task_describe, Date task_begin_time, Date task_end_time,
			String task_state, Integer user_id) {
		super();
		this.id = id;
		this.task_name = task_name;
		this.task_describe = task_describe;
		this.task_begin_time = task_begin_time;
		this.task_end_time = task_end_time;
		this.task_state = task_state;
		this.user_id = user_id;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTask_name() {
		return task_name;
	}
	public void setTask_name(String task_name) {
		this.task_name = task_name;
	}
	public String getTask_describe() {
		return task_describe;
	}
	public void setTask_describe(String task_describe) {
		this.task_describe = task_describe;
	}
	public Date getTask_begin_time() {
		return task_begin_time;
	}
	public void setTask_begin_time(Date task_begin_time) {
		this.task_begin_time = task_begin_time;
	}
	public Date getTask_end_time() {
		return task_end_time;
	}
	public void setTask_end_time(Date task_end_time) {
		this.task_end_time = task_end_time;
	}
	public String getTask_state() {
		return task_state;
	}
	public void setTask_state(String task_state) {
		this.task_state = task_state;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	@Override
	public String toString() {
		return "Task [id=" + id + ", task_name=" + task_name + ", task_describe=" + task_describe + ", task_begin_time="
				+ task_begin_time + ", task_end_time=" + task_end_time + ", task_state=" + task_state + ", user_id="
				+ user_id + "]";
	}
	
	
	
}
