package com.ctcc.dt_telecom.bean;

import java.io.Serializable;
import java.sql.Date;

/**
 * 封装有关用户的信息
 * @author wp
 *
 */
public class User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String username;//用户名
	private String gender;//性别：0 男 1女
	private String education;//学历
	private String major;//专业
	private Date birthday;//出生日期
	private	Date hiredate;//入职日期
	private String role;//角色0 Manager 1 Clerk 2 Employee
	private Integer superior_id;//上级 员工上级的id外键约束
	private String password;//密码
	private String position;//职位
	private String duty;//工作职责  职位描述
	private String experience;//工作经验

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public User(Integer id, String username, String gender, String education, String major, Date birthday,
			Date hiredate, String role, Integer superior_id, String password, String position, String duty,
			String experience) {
		super();
		this.id = id;
		this.username = username;
		this.gender = gender;
		this.education = education;
		this.major = major;
		this.birthday = birthday;
		this.hiredate = hiredate;
		this.role = role;
		this.superior_id = superior_id;
		this.password = password;
		this.position = position;
		this.duty = duty;
		this.experience = experience;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	

	/**
	 * 性别：0 男 1女
	 * @return
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * 性别：0 男 1女
	 * @param gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * 学历
	 * @return
	 */
	public String getEducation() {
		return education;
	}
	/**
	 * 学历
	 * @param education
	 */
	public void setEducation(String education) {
		this.education = education;
	}
	/**
	 * 专业
	 * @return
	 */
	public String getMajor() {
		return major;
	}
	/**
	 * 专业
	 * @param major
	 */
	public void setMajor(String major) {
		this.major = major;
	}
	/**
	 * 出生日期
	 * @return
	 */
	public Date getBirthday() {
		return birthday;
	}
	/**
	 * 出生日期
	 * @param birthday
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	/**
	 * 入职日期
	 * @return
	 */
	public Date getHiredate() {
		return hiredate;
	}
	/**
	 * 入职日期
	 * @param hiredate
	 */
	public void setHiredate(Date hiredate) {
		this.hiredate = hiredate;
	}
	/**
	 * 角色0 Manager 1 Clerk 2 Employee
	 * @return
	 */
	public String getRole() {
		return role;
	}
	/**
	 * 角色0 Manager 1 Clerk 2 Employee
	 * @param role
	 */
	public void setRole(String role) {
		this.role = role;
	}
	/**
	 * 上级的id
	 * @return
	 */
	public Integer getSuperior_id() {
		return superior_id;
	}

	/**
	 * 上级的id
	 * @param superior_id
	 */
	public void setSuperior_id(Integer superior_id) {
		this.superior_id = superior_id;
	}


	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * 职位
	 * @return
	 */
	public String getPosition() {
		return position;
	}
	/**
	 * 职位
	 * @param position
	 */
	public void setPosition(String position) {
		this.position = position;
	}
	/**
	 * 工作职责
	 * @return
	 */
	public String getDuty() {
		return duty;
	}
	/**
	 * 工作职责
	 * @param duty
	 */
	public void setDuty(String duty) {
		this.duty = duty;
	}
	/**
	 * 工作经验
	 * @return
	 */
	public String getExperience() {
		return experience;
	}
	/**
	 * 工作经验
	 * @param experience
	 */
	public void setExperience(String experience) {
		this.experience = experience;
	}


	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", gender=" + gender + ", education=" + education
				+ ", major=" + major + ", birthday=" + birthday + ", hiredate=" + hiredate + ", role=" + role
				+ ", superior_id=" + superior_id + ", password=" + password + ", position=" + position + ", duty="
				+ duty + ", experience=" + experience + "]";
	}
	
	


}
