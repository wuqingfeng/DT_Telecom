package com.ctcc.dt_telecom.bean;

import java.sql.Date;

/**
 * 封装计划相关的信息
 * @author wp
 *
 */
public class Plan {
	private Integer id;  //主键 自增
	private String plan_name; //计划名称
	private String plan_describe;  //计划描述
	private Date plan_begin_time; //计划开始时间
	private Date plan_end_time;//计划结束时间
	private String plan_state; //	计划状态 0为 未反馈，1已反馈
	private String feedback;//反馈信息 详细内容
	private Integer task_id;//任务id  task表的id  外键	
	public Plan() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Plan(Integer id, String plan_name, String plan_describe, Date plan_begin_time, Date plan_end_time,
			String plan_state, String feedback, Integer task_id) {
		super();
		this.id = id;
		this.plan_name = plan_name;
		this.plan_describe = plan_describe;
		this.plan_begin_time = plan_begin_time;
		this.plan_end_time = plan_end_time;
		this.plan_state = plan_state;
		this.feedback = feedback;
		this.task_id = task_id;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPlan_name() {
		return plan_name;
	}
	public void setPlan_name(String plan_name) {
		this.plan_name = plan_name;
	}
	public String getPlan_describe() {
		return plan_describe;
	}
	public void setPlan_describe(String plan_describe) {
		this.plan_describe = plan_describe;
	}
	public Date getPlan_begin_time() {
		return plan_begin_time;
	}
	public void setPlan_begin_time(Date plan_begin_time) {
		this.plan_begin_time = plan_begin_time;
	}
	public Date getPlan_end_time() {
		return plan_end_time;
	}
	public void setPlan_end_time(Date plan_end_time) {
		this.plan_end_time = plan_end_time;
	}
	public String getPlan_state() {
		return plan_state;
	}
	public void setPlan_state(String plan_state) {
		this.plan_state = plan_state;
	}
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	public Integer getTask_id() {
		return task_id;
	}
	public void setTask_id(Integer task_id) {
		this.task_id = task_id;
	}
	@Override
	public String toString() {
		return "Plan [id=" + id + ", plan_name=" + plan_name + ", plan_describe=" + plan_describe + ", plan_begin_time="
				+ plan_begin_time + ", plan_end_time=" + plan_end_time + ", plan_state=" + plan_state + ", feedback="
				+ feedback + ", task_id=" + task_id + "]";
	}
	
	

	


}
