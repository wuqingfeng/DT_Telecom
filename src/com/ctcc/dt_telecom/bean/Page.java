package com.ctcc.dt_telecom.bean;

import java.util.List;

/**
 * 处理分页
 * @author wp
 * @param <T>
 *
 */
public class Page<T> {
	

	private int pageNumber;//当前页面
	private int pageSize;//
//	private int totalPage;//总页数
	private int totalRecord;//总记录数
//	private int index;//索引计算所得的位置
	private List<T> data;//分页数据
	private String path;//保存请求地址的消息
	
	/**
	 * 当前页面
	 * @return
	 */
	public int getPageNumber() {
		if(pageNumber < 1) {
				return 1;
		}
		if(pageNumber > getTotalPage()) {
			return getTotalPage();
		}
			
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	/**
	 * 每页显示的个数
	 * @return
	 */
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	/**
	 * 计算总页数
	 * 总记录数 每页的条数  总页数
	 * 10		2		5
	 * 
	 * totalPage=totalRecord/ pageSize
	 * @return
	 */
	public int getTotalPage() {
		if (getTotalRecord() % getPageSize() == 0){
			return  getTotalRecord() / getPageSize();
		} else {
			return getTotalRecord() / getPageSize() + 1;
		}
	}
	
	/**
	 * 总记录数 
	 * @return
	 */
	public int getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}
	
	/**
	 * 计算index值
	 * pageNumber index pageSize
	 * 	1			0		3
	 * 	2			3		3
	 * 	3			6		3
	 * 
	 * index=(pageNumber -1) *pageSize;
	 * @return
	 */
	public int getIndex() {
		
		return (getPageNumber()-1)*getPageSize();
	}
	
	/**
	 * 从数据库找那个获取数据（数据库中）
	 * @return
	 */
	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}
	
	/**
	 * 保存请求地址的消息
	 * @return
	 */
	public String getPath() {
		return path;
	}
	/**
	 * 保存请求地址的消息
	 * @param path
	 */
	public void setPath(String path) {
		this.path = path;
	}
	
	
	

}
