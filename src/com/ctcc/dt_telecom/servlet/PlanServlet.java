package com.ctcc.dt_telecom.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ctcc.dt_telecom.bean.Page;
import com.ctcc.dt_telecom.bean.Plan;
import com.ctcc.dt_telecom.bean.Task;
import com.ctcc.dt_telecom.bean.User;
import com.ctcc.dt_telecom.service.PlanService;
import com.ctcc.dt_telecom.service.TaskService;
import com.ctcc.dt_telecom.service.UserService;
import com.ctcc.dt_telecom.service.impl.PlanServiceImpl;
import com.ctcc.dt_telecom.service.impl.TaskServiceImpl;
import com.ctcc.dt_telecom.service.impl.UserServiceImpl;
import com.ctcc.dt_telecom.utils.WEBUtils;

/**
 * 处理与plan有关的请求
 * @author wp
 *
 */
public class PlanServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private PlanService planService=new PlanServiceImpl();
	private UserService userService=new UserServiceImpl();
	private TaskService taskService=new TaskServiceImpl();
	
	/**
	 * 查询主管分配给  该用户 所有的任务
	 * 1.根据usrsid在dt_task中查询任务
	 * 2.根据usrsid在dt_user表中查询该用户的 主管的信息
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void findTask(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取已登录的用户
		User loginUser = WEBUtils.getLoginUser(request);
		//获取已登录用户的 上级的信息
		User userSuperior = userService.getUserByUserId(loginUser.getSuperior_id().toString());
		
		
		//将该用户存在list集合中
		List<User> userList = new ArrayList<User>();
		userList.add(loginUser);
		//获取当前页面
		String pageNumber = request.getParameter("pageNumber");
		//定义当前页面显示的条数
		int pageSize = 8;
		//设置要获取的任务状态 	任务状态 0未实施,1实施中,2已完成
		String task_state = "0,1,2";
		//获取任务
		Page<Task> page = taskService.getTaskListByUser(userList, pageNumber, pageSize, task_state);
		//获取当前路径
		String path = WEBUtils.getPath(request, response);
		//将路径存在page中
		page.setPath(path);
		//将page放在域中
		request.setAttribute("page", page);
		request.setAttribute("userSuperior", userSuperior);
		//转发到/task/mytask.jsp
		request.getRequestDispatcher("/task/mytask.jsp").forward(request, response);
		
	}
	
	/**
	 *任务详情  和计划的列表
	 *1.根据 taskId 获取task
	 *2. 根据task 获取 计划plan的集合
	 *3. 将已登录的 用户放在list中 传入
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void detailedTaskAndPlan(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取当前登录的用户
		User loginUser = WEBUtils.getLoginUser(request);
		//将loginUser放在list中
		List<User> userList=new ArrayList<>();
		userList.add(loginUser);
		//获取任务的Id 号码
		String taskId = request.getParameter("taskId");
		//根据taskId 获取task
		Task task = taskService.getTaskById(taskId);
		//根据taskId从plan表中获取 所有有关次task的plan
		List<Plan> planList = planService.getPlanListByTaskId(taskId);
		//将task 和plan的list 和 userList 放在域中
		request.setAttribute("task", task);
		request.setAttribute("planList", planList);
		request.setAttribute("userList", userList);
		//转发到/plan/myplan.jsp
		request.getRequestDispatcher("/plan/myplan.jsp").forward(request, response);
		
	}
	
	/**
	 *计划详情  选择某条计划  获取其详情
	 * 根据planId 获取 计划plan对象
	 * 根据plan.getTaskId 获取 计划task对象
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void detailedPlan(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String planId=request.getParameter("planId");
		Plan plan = planService.getPlanById(planId);
		Task task = taskService.getTaskById(plan.getTask_id().toString());
		request.setAttribute("plan", plan);
		request.setAttribute("task", task);
		//转发到feedbackplan.html
		request.getRequestDispatcher("/plan/feedbackplan.jsp").forward(request, response);
	}

	/**
	 * 制定计划
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void createPlan(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String taskId=request.getParameter("taskId");
	//	System.out.println(taskId);
		Task task = taskService.getTaskById(taskId);
	//	System.out.println(task);
		request.setAttribute("task", task);
		//转发到createnewplan.jsp
		request.getRequestDispatcher("/plan/createnewplan.jsp").forward(request, response);
	}

	/**
	 * 反馈计划  
	 * 1.将获取的计划反馈 保存到plan表中  也就是更新plan表  根据id
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void updatePlan(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取计划
		Plan plan = WEBUtils.paramToBean(request, new Plan());
		//System.out.println(plan);
		//更新计划
		int i = planService.updatePlan(plan);
	//	System.out.println(i);
		//重定向到PlanServlet?method=detailedTaskAndPlan 将taskId=plan.getTask_id();传过去
		response.sendRedirect(request.getContextPath()+"/PlanServlet?method=detailedTaskAndPlan&taskId="+plan.getTask_id());
	}
	
	/**
	 * 将计划保存到数据库
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void savePlan(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取计划
		Plan plan = WEBUtils.paramToBean(request, new Plan());
		System.out.println(plan);
		//更新计划
		int i = planService.savePlan(plan);
	//	System.out.println(i);
		//重定向到PlanServlet?method=detailedTaskAndPlan 将taskId=plan.getTask_id();传过去
		response.sendRedirect(request.getContextPath()
				+"/PlanServlet?method=detailedTaskAndPlan&taskId="+plan.getTask_id());
	}
	
	
	/**
	 * 删除计划
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void deletePlan(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取plan的id
		String planIds = request.getParameter("planId");
		String taskId = request.getParameter("taskId");
		int i = planService.deletePlan(planIds);
		//System.out.println(i);
		//重定向到PlanServlet?method=detailedTaskAndPlan 将taskId=plan.getTask_id();传过去
		response.sendRedirect(request.getContextPath()
				+"/PlanServlet?method=detailedTaskAndPlan&taskId="+taskId);

	}
	
	/**
	 * 查询计划
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void findTaskList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取已登录的用户
		User loginUser = WEBUtils.getLoginUser(request);
		//根据已登录的用户获取 属于该用户的所有任务
		List<Task> listTask = taskService.getTaskListByUser(loginUser);
		//放在域中
		request.setAttribute("listTask", listTask);
		//转发到/plan/inqurieplan.jsp
		request.getRequestDispatcher("/plan/inqurieplan.jsp").forward(request, response);
	}
	
	/**
	 * 查找计划
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void findPlan(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取查询的时间
		Map<String, Long> date = WEBUtils.getQueryDate(request);
		//获取查询条件
		Plan plan = WEBUtils.paramToBean(request, new Plan());
		
		List<Plan> listPlan = planService.getPlanListByOther(plan,date);
		//将list放在域中
		request.setAttribute("listPlan", listPlan);
		/*//转发到原页面
		request.getRequestDispatcher("/plan/inqurieplan.jsp").forward(request, response);*/
		
		//调用findTaskList(request, response)  
		findTaskList(request, response);
	}

	


}
