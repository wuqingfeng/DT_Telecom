package com.ctcc.dt_telecom.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ctcc.dt_telecom.bean.User;
import com.ctcc.dt_telecom.service.UserService;
import com.ctcc.dt_telecom.service.impl.UserServiceImpl;
import com.ctcc.dt_telecom.utils.WEBUtils;
import com.google.gson.Gson;

/**
 * Servlet implementation class ManagerServlet
 */
public class ManagerServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	
	private UserService userService=new UserServiceImpl();

    /**
     * 添加员工
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
	protected void addUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//获取表单提交过来的user值
		User user=WEBUtils.paramToBean(request, new User());
	
		//储存User 
		userService.addUser(user);
		//重定向到首页
		response.sendRedirect(request.getContextPath()+"/ManagerServlet?method=userList");
	}


	/**
	 * 删除员工
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException  deleteUser
	 */
	protected void deleteUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取id的集合
		String userIds=request.getParameter("userIds");
		//System.out.println(userIds);
		//删除数据
		int count=userService.deleteUser(userIds);
		//重定向到userList
		response.sendRedirect(request.getContextPath()+"/ManagerServlet?method=userList");	
	}
	
	protected void deleteUser1(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取id的集合
		String userIds=request.getParameter("userIds");
		System.out.println(userIds);
		//删除数据
		int count=userService.deleteUser(userIds);
		//将请求发给客户端  将已删除的条数发到客户端
		  response.getWriter().print(count);

		
	}
	
	/**
	 * 分配员工
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void distrubutionUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取User对象
		User user=WEBUtils.paramToBean(request, new User());
		//System.out.println(user);
		//分配员工 并更新数据库信息
		int count =userService.distributionUser(user);
	//	System.out.println(count);
		//重定向到主页
		response.sendRedirect(request.getContextPath()+"/ManagerServlet?method=userList");
	}
	
	/**
	 * 人员列表
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void userList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取人员的列表
		List<User> userList = userService.getUserList();
		//放到request域中
		request.setAttribute("userList", userList);
		//转发到人员管理页面.
		request.getRequestDispatcher("/person/user.jsp").forward(request, response);
	}
	
	/**
	 * 根据用户的id查看用户的详细信息
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void findUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取用户的id属性
		String userId=request.getParameter("userId");
		String supername=new String(request.getParameter("supername").getBytes("ISO-8859-1"),"UTF-8");
		//System.out.println(supername);
		//查找用户
		User user=userService.getUserByUserId(userId);
		
		List<User> superiorList = userService.getSuperiorByRole();
	//	System.out.println(user);
		//将user放在request域中
		request.setAttribute("user", user);
		request.setAttribute("superiorList", superiorList);
		
		//选中数据中的上级的姓名，
		request.setAttribute("supername", supername);
		//转发到person/distributionworkerInfo.jsp
		request.getRequestDispatcher("/person/distributionworkerInfo.jsp").forward(request, response);
	
	}

}
