package com.ctcc.dt_telecom.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpCookie;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.jws.soap.SOAPBinding.Use;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;

import com.ctcc.dt_telecom.bean.User;
import com.ctcc.dt_telecom.service.UserService;
import com.ctcc.dt_telecom.service.impl.UserServiceImpl;
import com.ctcc.dt_telecom.utils.WEBUtils;

/**
 * 处理用户的登录和登出请求
 * 
 * @author wp
 *
 */
public class UserServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private UserService userService = new UserServiceImpl();

	/**
	 * 用户登录
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void login(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取session
		HttpSession session = request.getSession();
		// 获取是否选择记住密码
		String ck = request.getParameter("ck");
		// 获取提交的user对象
		User user = WEBUtils.paramToBean(request, new User());
		// 从数据库中验证user是否存在
		//System.out.println(user);
		User loginUser = userService.login(user);
		if (loginUser != null) {
			// 登录成功
			// 记住密码
			if ("on".equals(ck)) {
				//URLEncoder.encode(user.getUsername(), "utf-8");
				//存入角色 代号  角色0 Manager 1 Clerk 2 Employee  存入用户名  密码
				//例如 0-admin-123
				Cookie cookie = new Cookie("userinfo"
						, user.getRole()+"-"+URLEncoder.encode(user.getUsername(), "utf-8")+"-"
				+ URLEncoder.encode(user.getPassword(), "utf-8"));
				response.addCookie(cookie);
				
				
				cookie.setMaxAge(-1);// 永久有效 直到浏览器关闭
				
				/*String  cookieValue = URLDecoder.decode(cookie.getValue(), "UTF-8");
				System.out.println(cookieValue);*/
			
			}

			// 进loginUser 保存到session中
			session.setAttribute("loginUser", loginUser);
			SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd  HH:mm");
			String date = simpleDateFormat.format(new Date());
			session.setAttribute("date", date);
			// 重定向到index.jap
			response.sendRedirect(request.getContextPath() + "/index.jsp");
			// System.out.println("登录成功");
		} else {
			// 用户名或者密码错误 设置错误提示 转发到登录页面
			request.setAttribute("msg", "用户名或者密码错误");
			request.getRequestDispatcher("/login.jsp").forward(request, response);

		}
	}

	/**
	 * 注销登录
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void logout(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session=request.getSession();
		//强制session失效
		session.invalidate();
		//重定向到登录页面
		response.sendRedirect(request.getContextPath()+"/login.jsp");
		
		
	}

}
