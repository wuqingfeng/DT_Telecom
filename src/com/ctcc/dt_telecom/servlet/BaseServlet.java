package com.ctcc.dt_telecom.servlet;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 专门被其他的servlet所继承
 * @author wp
 *
 */
public class BaseServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取method
		String methodName=request.getParameter("method");
	//	System.out.println(methodName);
		//获取当前方法的class
		Class<?> clazz=this.getClass();
		try {
			
			//获取当前class的所以方法
			Method method = clazz.getDeclaredMethod(methodName,HttpServletRequest.class,HttpServletResponse.class);
			//取消 Java 语言访问检查
			method.setAccessible(true);
			//调用次method的底层方法   invoke(底层方法所在的对象，参数...)
			method.invoke(this,request,response);
		
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		doGet(request, response);
	}
	
	

}
