package com.ctcc.dt_telecom.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ctcc.dt_telecom.bean.Page;
import com.ctcc.dt_telecom.bean.Plan;
import com.ctcc.dt_telecom.bean.Task;
import com.ctcc.dt_telecom.bean.User;
import com.ctcc.dt_telecom.service.PlanService;
import com.ctcc.dt_telecom.service.TaskService;
import com.ctcc.dt_telecom.service.UserService;
import com.ctcc.dt_telecom.service.impl.PlanServiceImpl;
import com.ctcc.dt_telecom.service.impl.TaskServiceImpl;
import com.ctcc.dt_telecom.service.impl.UserServiceImpl;
import com.ctcc.dt_telecom.utils.WEBUtils;

public class TaskServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private TaskService taskService = new TaskServiceImpl();
	private UserService userService = new UserServiceImpl();
	private PlanService planService = new PlanServiceImpl();

	/**
	 * 制定任务
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void createTask(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//获取已登录用户的信息
		User loginUser=WEBUtils.getLoginUser(request);
		//获取已登录用户的 下属
		List<User> list = userService.getUserBySuperiorId(loginUser);
		//放在request域中
		request.setAttribute("list", list);
		//转发到/task/newtask.jsp
		request.getRequestDispatcher("/task/newtask.jsp").forward(request, response);
		
	}
	/**
	 * 保存任务
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void saveTask(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//获取已输入的任务信息
		Task task = WEBUtils.paramToBean(request, new Task());
	//	Task task = DateTimeConverter.transMap2Bean(request, new Task());
	//	System.out.println(task);
		//保存任务信息
		taskService.saveTask(task);
		//System.out.println(count);
		//重定向到查看任务页面task/taskinformation.html
		response.sendRedirect(request.getContextPath()
				+"/TaskServlet?method=showTask&target=taskinformation&task_state=0,1,2");
	}

	/**
	 * 查看任务
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void showTask(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//获取要 跳转的页面地址
		String target=request.getParameter("target");
	//	System.out.println("target: "+target);
		//获取任务状态
		String task_state=request.getParameter("task_state");
		
		//获取已登录用户的信息
		User loginUser=WEBUtils.getLoginUser(request);	
		
		
		//获取页面
		String pageNumber=request.getParameter("pageNumber");
		//定义没也显示的数据量
		int pageSize=8;
		//查找该用户的所有 下属
		List<User> userList = userService.getUserBySuperiorId(loginUser);
		//获取任务页面
		Page<Task> page = taskService.getTaskListByUser(userList, pageNumber, pageSize,task_state);
			
		//获取请求的地址
		page.setPath(WEBUtils.getPath(request, response));
		//將page放在域中
		request.setAttribute("page", page);
		request.setAttribute("userList", userList);
		
		//获取请求头
	//	String referer = request.getHeader("referer");
	//	System.out.println(referer);
		//转发到查看task/taskinformation.html
		request.getRequestDispatcher("/task/"+target+".jsp").forward(request, response);
		

	}
	/**
	 * 详细 任务信息
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void detailedtaskinformation(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//获取要跳转的页面
		String target=request.getParameter("target");
		//获取任务的id
		String taskId=request.getParameter("taskId");
		
		User loginUser=WEBUtils.getLoginUser(request);	
		//查找该用户的所有 下属
		List<User> userList = userService.getUserBySuperiorId(loginUser);
		
		
		//根据id获取该任务的详细情况
		Task task = taskService.getTaskById(taskId);
		//根据 taskId 获取该任务的所有计划
		List<Plan> planList = planService.getPlanListByTaskId(taskId);
		//根据task.getUser_id()获取的user实施人的信息
		task.getUser_id();
		User task_user = userService.getUserByUserId(task.getUser_id().toString());
		//放在reques域中
		request.setAttribute("task", task);
		request.setAttribute("planList", planList);
		request.setAttribute("task_user", task_user);
		request.setAttribute("userList", userList);
		//转发到detailedtaskinformation
		request.getRequestDispatcher("/task/"+target+".jsp").forward(request, response);

	}
	
	/**
	 * 详细 计划信息
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void detailedplaninformation(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String planId=request.getParameter("planId");
		//更具id获取计划详情
		Plan plan = planService.getPlanById(planId);
		Task task = taskService.getTaskById(plan.getTask_id().toString());
		//放在request域中 
		request.setAttribute("plan", plan);
		request.setAttribute("task", task);
		//转发到plandetailedinformation.jsp
		request.getRequestDispatcher("/task/plandetailedinformation.jsp").forward(request, response);

	}
	

	/**
	 * 调整任务
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void updateTask(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//获取要跳转的页面信息
		String target=request.getParameter("target");
		String task_state=request.getParameter("task_state1");
		//获取已输入的任务信息
		Task task = WEBUtils.paramToBean(request, new Task());
		//System.out.println(task);
		//保存任务信息
		taskService.updateTask(task);
		//System.out.println(i);
		//重定向到查看任务页面task/taskinformation.html
		response.sendRedirect(request.getContextPath()
				+"/TaskServlet?method=showTask&target="+target+"&task_state="+task_state);
		
	}

	/**
	 * 跟踪任务
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void trackTask(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}
	
	/**
	 * 删除任务
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void deleteTask(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String taskId=request.getParameter("taskId");
		int i = taskService.deleteTask(taskId);
		System.out.println(i);
		response.sendRedirect(request.getContextPath()+"/TaskServlet?"
				+ "method=showTask&target=taskadjust&task_state=0");
		
	}
	


	/**
	 * 查看人员
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void findUser(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		//获取已登录用户的信息
		User loginUser=WEBUtils.getLoginUser(request);
		
		//获取页面
		String pageNumber=request.getParameter("pageNumber");
		
		//定义没也显示的数据量
		int pageSize=8;
		Page<User> page = userService.getUserBySuperiorId(loginUser, pageNumber, pageSize);
	
		//获取请求的地址
		page.setPath(WEBUtils.getPath(request, response));
		//將page放在域中
		request.setAttribute("page", page);
		//转发到查看人员页面
		request.getRequestDispatcher("/person/worker.jsp").forward(request, response);

	}

	 
	/**
	 * 员工详细信息显示
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void workerInfomation(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
		//获取id地址
		 String userId=request.getParameter("userId");

		 //向数据库中查询user对象
		 User user = userService.getUserByUserId(userId);

		 //放在request域中
		 request.setAttribute("user", user);
		 //转发到workerInfomation.jsp页面
		 request.getRequestDispatcher("/person/workerInfomation.jsp").forward(request, response);
		 
	}
}
